package com.cybilltech.happygreen.Models;


import java.io.Serializable;

public class OrderModel implements Serializable {

    public int orderId;
    public String productIdList;
    public String ProductNameList;
    public String productQtyList;
    public String productQtyList2;
    public String orderType;
    public String paymentType;
    public String orderStatus;
    public int userId;
    public String userName;
    public String userPhone;
    public String userAddress;
    public String userPinCode;
    public String orderDate;
    public String cancelReason;
    public Double OrderAmount;

}
