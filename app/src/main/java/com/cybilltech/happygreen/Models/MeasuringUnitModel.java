package com.cybilltech.happygreen.Models;


import java.io.Serializable;

public class MeasuringUnitModel implements Serializable {

    public int measCatId;
    public String measCatName;
    public Double measCatValues;

    public MeasuringUnitModel(int measCatId, String measCatName, Double measCatValues) {
        this.measCatId = measCatId;
        this.measCatName = measCatName;
        this.measCatValues = measCatValues;
    }

    public MeasuringUnitModel(){}
}
