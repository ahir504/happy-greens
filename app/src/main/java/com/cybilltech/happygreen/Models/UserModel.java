package com.cybilltech.happygreen.Models;


import java.io.Serializable;

public class UserModel implements Serializable {

    public  int userId;
    public String first_name;
    public String last_name;
    public String phone;
    public String email;
    public String password;
    public String address;
    public String pincode;

}
