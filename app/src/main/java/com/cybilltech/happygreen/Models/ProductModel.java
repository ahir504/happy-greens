package com.cybilltech.happygreen.Models;


import java.io.Serializable;
import java.util.ArrayList;

public class ProductModel implements Serializable {

    public int productId;
    public double itemSize;
    public int catId;
    public String productName;
    public String qtyName;
    public String productDescription;
    public double productPrice;
    public double productQuantity;
    public double productQuantityLeft;
    public String productImage;
    public String createdAt;
    public int path;
    public int productUnitSizeCatId;
    public int productUnitCat;
    public String deliveryType;
    public double productDiscount;
    public String productShippingCharge;
    public String validity;
    public int adpterSelectedPos = 0;

    public ArrayList<MeasuringUnitModel> measuringList;


    public ProductModel(){
    }

    public ProductModel(int productId, String productName, double productPrice, int path, String deliveryType, double productDiscount, String validity){

        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.path = path;
        this.deliveryType = deliveryType;
        this.productDiscount = productDiscount;
        this.validity = validity;

    }

}
