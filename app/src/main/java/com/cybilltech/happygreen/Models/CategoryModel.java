package com.cybilltech.happygreen.Models;



import java.io.Serializable;
import java.util.ArrayList;

public class CategoryModel implements Serializable {
    public int catId;
    public String catName;
    public ArrayList<ProductModel> productList;

    public CategoryModel(){}

    public CategoryModel(int catId, String catName, ArrayList<ProductModel> productList){
        this.catName = catName;
        this.catId = catId;
        this.productList = productList;
    }
}
