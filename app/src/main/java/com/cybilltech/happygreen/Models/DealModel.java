package com.cybilltech.happygreen.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class DealModel implements Serializable {
    public int dealId;
    public String dealName;
    public ArrayList<ProductModel> arrayList;
    public String dealImg;

    public DealModel() {
    }

    public DealModel(int dealId, String dealName, ArrayList<ProductModel> arrayList) {
        this.dealId = dealId;
        this.dealName = dealName;
        this.arrayList = arrayList;
    }
}
