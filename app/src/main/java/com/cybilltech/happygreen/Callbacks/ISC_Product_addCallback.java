package com.cybilltech.happygreen.Callbacks;

import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.ProductModel;

public interface ISC_Product_addCallback {

    void onClick(ProductModel productModel, double amount, MeasuringUnitModel measuringUnitModel);


}
