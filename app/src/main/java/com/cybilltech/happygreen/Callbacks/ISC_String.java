package com.cybilltech.happygreen.Callbacks;


public interface ISC_String {

    void onSuccess(String msg);
    void onError(String error);
}
