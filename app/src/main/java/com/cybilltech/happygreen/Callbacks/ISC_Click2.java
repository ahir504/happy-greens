package com.cybilltech.happygreen.Callbacks;

import android.view.View;

import com.cybilltech.happygreen.Models.ProductModel;

public interface ISC_Click2 {

    void onClick(View view, ProductModel productModel);

}
