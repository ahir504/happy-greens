package com.cybilltech.happygreen.Callbacks;

import android.view.View;

public interface ISC_AlertClick {

    void onPositiveButtonClicked(View view);
    void onNegativeButtonClicked(View view);
}
