package com.cybilltech.happygreen.Callbacks;


import com.cybilltech.happygreen.Models.ProductModel;

public interface ISC_ProductItem {

    void onSuccess(ProductModel productModel);
    void onError(String error);
}
