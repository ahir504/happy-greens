package com.cybilltech.happygreen.Callbacks;

import android.view.View;

public interface ISC_AlertClickPositiveOnly {

    void onPositiveButtonClicked(View view);
}
