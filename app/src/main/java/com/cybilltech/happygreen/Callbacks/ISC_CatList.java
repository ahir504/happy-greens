package com.cybilltech.happygreen.Callbacks;

import com.cybilltech.happygreen.Models.CategoryModel;

import java.util.ArrayList;

public interface ISC_CatList {
    void onSuccess(ArrayList<CategoryModel> categoryList);
    void onError(String error);

}
