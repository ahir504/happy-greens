package com.cybilltech.happygreen.Callbacks;

import com.cybilltech.happygreen.Models.DealModel;

import java.util.ArrayList;

public interface ISC_DealList {
    void onSuccess(ArrayList<DealModel> dealModels);
    void onError(String error);
}
