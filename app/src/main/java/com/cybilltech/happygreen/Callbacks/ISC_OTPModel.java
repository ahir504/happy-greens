package com.cybilltech.happygreen.Callbacks;


import com.cybilltech.happygreen.Models.OTPModel;

public interface ISC_OTPModel {

    void onSuccess(OTPModel otpModel);
    void onError(String error);

}
