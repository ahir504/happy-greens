package com.cybilltech.happygreen.Callbacks;


import com.cybilltech.happygreen.Models.OrderModel;

import java.util.ArrayList;

public interface ISC_OrderList {

    void onSuccess(ArrayList<OrderModel> orderList);
    void onError(String error);

}
