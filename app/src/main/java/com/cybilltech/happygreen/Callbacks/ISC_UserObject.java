package com.cybilltech.happygreen.Callbacks;



import com.cybilltech.happygreen.Models.UserModel;

public interface ISC_UserObject {

    void onSuccess(UserModel userModel);
    void onError(String error);

}
