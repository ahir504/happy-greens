package com.cybilltech.happygreen.Callbacks;

public interface ISC_Boolean {

    void onSuccess(boolean isAvailable );
    void onError(String error);

}
