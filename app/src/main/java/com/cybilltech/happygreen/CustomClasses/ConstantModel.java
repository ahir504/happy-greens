package com.cybilltech.happygreen.CustomClasses;



public class ConstantModel {

    public final static String activityName = "activityName";
    public final static String action = "action_string";
    public final static String Variable = "Variable";
    public final static String Fixed = "Fixed";

    public final static String standardOrder = "Today Order";
    public final static String advanceOrder = "Evening Order";
    public final static String unknownOrder = "Unknown Order";

    public final static String pending = "Order Booked";
    public final static String delivered = "Delivered";
    public final static String cancelled = "Cancelled";

    public final static int SuccessAlertDialog = 1;
    public final static int FailedAlertDialog = 2;
    public final static int LogoutAlertDialog = 3;


}
