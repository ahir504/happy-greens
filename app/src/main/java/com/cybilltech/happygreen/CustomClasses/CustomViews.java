package com.cybilltech.happygreen.CustomClasses;


import android.app.AlertDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_AlertClick;
import com.cybilltech.happygreen.Callbacks.ISC_AlertClickPositiveOnly;
import com.cybilltech.happygreen.Callbacks.ISC_Click;
import com.cybilltech.happygreen.Callbacks.ISC_ClickReturnMsg;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.R;

public class CustomViews {

    public static void showDialogwithEdit(final Context context, String title, final ISC_ClickReturnMsg callback){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.custom_alert_edittext, null);

        TextView tvTitle = view.findViewById(R.id.custom_alert_edittext_tv_title);
        final EditText etFlatNo = view.findViewById(R.id.custom_alert_edittext_et_flatNo);
        final EditText etSocietyName = view.findViewById(R.id.custom_alert_edittext_et_societyName);
        final EditText etSectorNo = view.findViewById(R.id.custom_alert_edittext_et_sectorNo);
        final EditText etReamrks = view.findViewById(R.id.custom_alert_edittext_et_remarks);
        Button button = view.findViewById(R.id.custom_alert_edittext_btnPositive);
        ImageView closeBtn = view.findViewById(R.id.custom_alert_edittext_closeBtn);

        tvTitle.setText(StaticClass.nullChecker(title));

        builder.setCancelable(false);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etFlatNo.getText().toString().length() == 0){
                    etFlatNo.setError("Please enter a valid Flat number.!!!");
                    return;
                }else if (etSocietyName.getText().toString().length() < 2){
                    etSocietyName.setError("Please enter a valid Society Name.!!!");
                    return;
                }else if (etSectorNo.getText().toString().length() == 0){
                    etSectorNo.setError("Please enter a valid Sector Number.!!!");
                    return;
//                }else if (etPincode.getText().toString().length() < 5){
//                    etPincode.setError("Please enter a valid Pincode.!!!");
//                    return;
                }else {

                    String add = "Flat NO:  "+ etFlatNo.getText().toString().trim() + "\n"
                                 + "Society Name: "+ etSocietyName.getText().toString().trim() + "\n"
                                 + "Sector No.: "+ etSectorNo.getText().toString().trim() + "\n"
                                 + "Noida";

                    if (etReamrks.getText().toString().trim().length() > 1){

                        add = add + "\n Remarks: " + etReamrks.getText().toString();

                    }
                    alertDialog.dismiss();
                    callback.onClick(v, add, "");
                }
            }
        });

    }

    public static void alertDialogToast(Context context, String title, int type, final ISC_AlertClick callback){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View alertView =  ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_alert_success, null);
        Button btnItem = alertView.findViewById(R.id.custom_alert_toast_body_btn_btn1);
        Button btnItem2 = alertView.findViewById(R.id.custom_alert_toast_body_btn_btn2);
        TextView tvTitle = alertView.findViewById(R.id.custom_alert_toast_body_tv_title);
        LinearLayout layoutHeader = alertView.findViewById(R.id.custom_alert_toast_ll_header);
        ImageView ivHeaderIcon = alertView.findViewById(R.id.custom_alert_toast_header_iv_imgView);

        if (type == ConstantModel.LogoutAlertDialog){
            layoutHeader.setBackground(context.getResources().getDrawable(R.drawable.ring_red));
            ivHeaderIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_error_outline));
            tvTitle.setText(StaticClass.nullChecker(title));
            btnItem.setBackground(context.getResources().getDrawable(R.drawable.round_ripple_red));
            btnItem2.setBackground(context.getResources().getDrawable(R.drawable.round_corner_primary));
            btnItem.setText("Yes");
            btnItem2.setText("No");
            btnItem2.setVisibility(View.VISIBLE);
        }else {

            layoutHeader.setBackground(context.getResources().getDrawable(R.drawable.ring_primary));
            ivHeaderIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
            tvTitle.setText(StaticClass.nullChecker(title));
            btnItem.setBackground(context.getResources().getDrawable(R.drawable.round_corner_primary));
            btnItem2.setBackground(context.getResources().getDrawable(R.drawable.round_ripple_red));
            btnItem.setText("Yes");
            btnItem2.setText("No");
            btnItem2.setVisibility(View.VISIBLE);
        }

        builder.setView(alertView);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        btnItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                callback.onPositiveButtonClicked(v);
            }
        });

        btnItem2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                callback.onNegativeButtonClicked(v);
            }
        });



    }

    public static void alertDialogToast(Context context, String title, int type){

        if (title == null ||title.trim().length() == 0){
            title = "No internet connectivity, Please check your internet connectivity !!!!";
        }else if (title.contains( "java.net.UnknownHostException:")){
            title = "No internet connectivity, Please check your internet connectivity !!!!";
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View alertView =  ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_alert_success, null);
        Button btnItem = alertView.findViewById(R.id.custom_alert_toast_body_btn_btn1);
        Button btnItem2 = alertView.findViewById(R.id.custom_alert_toast_body_btn_btn2);
        TextView tvTitle = alertView.findViewById(R.id.custom_alert_toast_body_tv_title);
        LinearLayout layoutHeader = alertView.findViewById(R.id.custom_alert_toast_ll_header);
        ImageView ivHeaderIcon = alertView.findViewById(R.id.custom_alert_toast_header_iv_imgView);

        if (type == ConstantModel.SuccessAlertDialog){

            layoutHeader.setBackground(context.getResources().getDrawable(R.drawable.ring_primary));
            ivHeaderIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
            tvTitle.setText(StaticClass.nullChecker(title));
            btnItem.setBackground(context.getResources().getDrawable(R.drawable.round_corner_primary));
            btnItem.setText("OK");
            btnItem2.setVisibility(View.GONE);
        }else if(type == ConstantModel.FailedAlertDialog){
            layoutHeader.setBackground(context.getResources().getDrawable(R.drawable.ring_red));
            ivHeaderIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
            tvTitle.setText(StaticClass.nullChecker(title));
            btnItem.setBackground(context.getResources().getDrawable(R.drawable.round_ripple_red));
            btnItem.setText("OK");
            btnItem2.setVisibility(View.GONE);
        }else {
            return;
        }

        builder.setView(alertView);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        btnItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });



    }

    public static void alertDialogToast(Context context, String title, int type, final ISC_AlertClickPositiveOnly callback){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View alertView =  ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_alert_success, null);
        Button btnItem = alertView.findViewById(R.id.custom_alert_toast_body_btn_btn1);
        Button btnItem2 = alertView.findViewById(R.id.custom_alert_toast_body_btn_btn2);
        TextView tvTitle = alertView.findViewById(R.id.custom_alert_toast_body_tv_title);
        LinearLayout layoutHeader = alertView.findViewById(R.id.custom_alert_toast_ll_header);
        ImageView ivHeaderIcon = alertView.findViewById(R.id.custom_alert_toast_header_iv_imgView);

        if (type == ConstantModel.SuccessAlertDialog){
            layoutHeader.setBackground(context.getResources().getDrawable(R.drawable.ring_primary));
            ivHeaderIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
            tvTitle.setText(StaticClass.nullChecker(title));
            btnItem.setBackground(context.getResources().getDrawable(R.drawable.round_corner_primary));
            btnItem.setText("OK");
            btnItem2.setVisibility(View.GONE);
        }else if(type == ConstantModel.FailedAlertDialog){
            layoutHeader.setBackground(context.getResources().getDrawable(R.drawable.ring_red));
            ivHeaderIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
            tvTitle.setText(StaticClass.nullChecker(title));
            btnItem.setBackground(context.getResources().getDrawable(R.drawable.round_ripple_red));
            btnItem.setText("OK");
            btnItem2.setVisibility(View.GONE);
        }else {
            return;
        }

        builder.setView(alertView);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        btnItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                callback.onPositiveButtonClicked(v);
            }
        });



    }
}
