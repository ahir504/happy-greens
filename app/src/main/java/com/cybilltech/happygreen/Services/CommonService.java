package com.cybilltech.happygreen.Services;


import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cybilltech.happygreen.Callbacks.ISC_Boolean;
import com.cybilltech.happygreen.Callbacks.ISC_CatList;
import com.cybilltech.happygreen.Callbacks.ISC_OrderList;
import com.cybilltech.happygreen.Callbacks.ISC_ProductItem;
import com.cybilltech.happygreen.Callbacks.ISC_String;
import com.cybilltech.happygreen.Callbacks.ISC_StringString;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.Models.CategoryModel;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.OrderModel;
import com.cybilltech.happygreen.Models.ProductModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommonService {

    private Context context;
    private RequestQueue requestQueue;
    private String path = "";
    private String url = "";

    public CommonService(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
        path = StaticClass.BASE_URL + "common/";
    }

    public void saveFeedback(final String feedback, final ISC_String callback){
        url = path + "feedback.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String msg = jsonObject.getString("msg");

                    if (msg.equalsIgnoreCase("s")){
                        callback.onSuccess(msg);
                    }else if (msg.equalsIgnoreCase("f")){
                        callback.onSuccess(msg);
                    }else {
                        callback.onError(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userId", MySharedPref.getUserId(context) + "");
                params.put("feedback", feedback);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
