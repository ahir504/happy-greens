package com.cybilltech.happygreen.Services;


import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cybilltech.happygreen.Callbacks.ISC_Boolean;
import com.cybilltech.happygreen.Callbacks.ISC_CatList;
import com.cybilltech.happygreen.Callbacks.ISC_DealList;
import com.cybilltech.happygreen.Callbacks.ISC_OrderList;
import com.cybilltech.happygreen.Callbacks.ISC_String;
import com.cybilltech.happygreen.Callbacks.ISC_StringString;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.Models.CategoryModel;
import com.cybilltech.happygreen.Models.DealModel;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.OrderModel;
import com.cybilltech.happygreen.Models.ProductModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ManageProductService {

    private Context context;
    private RequestQueue requestQueue;
    private String path = "";
    private String url = "";

    public ManageProductService(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
        path = StaticClass.BASE_URL + "manage-products/";
    }

    public void getDealList(final ISC_DealList dealList){
        url=path+"dealList.php";
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONArray jsonArray =new JSONArray(response);

                            if (jsonArray.length()>0){

                                ArrayList<DealModel> dealModels = new ArrayList<>();


                                for (int q = 0; q < jsonArray.length(); q++){
                                    JSONObject jsonObject=jsonArray.getJSONObject(q);
                                    DealModel dealModel =new DealModel();
                                    dealModel.dealId = jsonObject.getInt("dealId");
                                    dealModel.dealName = StaticClass.nullChecker(jsonObject.getString("dealName"));
                                    dealModel.dealImg =StaticClass.nullChecker(jsonObject.getString("dealImg"));


                                    JSONArray productJsonArray = jsonObject.getJSONArray("product");

                                    if (productJsonArray.length() > 0){
                                        dealModel.arrayList = new ArrayList<>();
                                        for (int b = 0; b < productJsonArray.length(); b++){
                                            ProductModel productModel = new ProductModel();
                                            JSONObject oo = productJsonArray.getJSONObject(b);
                                            productModel.productId = oo.getInt("productId");
                                            productModel.catId = oo.getInt("catId");
                                            productModel.productUnitSizeCatId = oo.getInt("productUnitSizeCatId");
                                            productModel.productName = StaticClass.nullChecker(oo.getString("productName"));
                                            productModel.productDescription = StaticClass.nullChecker(oo.getString("productDescription"));
                                            productModel.productPrice = oo.getDouble("productPrice");
                                            productModel.productQuantityLeft = oo.getDouble("productQuantity");

                                            String pdc = StaticClass.nullChecker("productDiscount");

                                            if (pdc== null || pdc.isEmpty()){
                                                productModel.productDiscount = 0.0;
                                            }else {
                                                try {
                                                    productModel.productDiscount = Double.parseDouble(pdc);
                                                }catch (Exception e){
                                                    productModel.productDiscount = 0.0;
                                                }
                                            }

                                            productModel.productDiscount = oo.getDouble("productDiscount");
                                            productModel.productImage = StaticClass.nullChecker(oo.getString("productImage"));

                                            JSONArray measArray = oo.getJSONArray("measuringUnits");

                                            if (measArray.length() > 0) {
                                                productModel.measuringList = new ArrayList<>();
                                                for (int c = 0; c < measArray.length(); c++) {
                                                    MeasuringUnitModel measuringUnitModel = new MeasuringUnitModel();
                                                    JSONObject ooo = measArray.getJSONObject(c);
                                                    measuringUnitModel.measCatId = ooo.getInt("measCatId");
                                                    measuringUnitModel.measCatValues = ooo.getDouble("measCatValues");
                                                    measuringUnitModel.measCatName = ooo.getString("measCatName");
                                                    productModel.measuringList.add(measuringUnitModel);
                                                }
                                            }
                                            dealModel.arrayList.add(productModel);
                                        }
                                    }else {
                                        dealModel.arrayList = new ArrayList<>();
                                    }
                                    dealModels.add(dealModel);

                                }

                                dealList.onSuccess(dealModels);
                            }else {
                                dealList.onSuccess(new ArrayList<DealModel>());
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                            dealList.onError(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    if (error.networkResponse.statusCode == 301){

                        String s = error.networkResponse.headers.get("Location");


                        dealList.onError(error.getLocalizedMessage());
                    }
                }catch (Exception e){
                    dealList.onError("");
                }
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    public void getCategoryList(final ISC_CatList callback){
        url = path + "categoryList.php";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try
                {

                    JSONArray jsonArray = new JSONArray(response);

                    if (jsonArray.length() > 0){
                        ArrayList<CategoryModel> categoryModels = new ArrayList<>();

                        for (int a = 0; a < jsonArray.length(); a++){
                            JSONObject o = jsonArray.getJSONObject(a);
                            CategoryModel categoryModel = new CategoryModel();
                            categoryModel.catId = o.getInt("catId");
                            categoryModel.catName = StaticClass.nullChecker(o.getString("catName"));

                            JSONArray productJsonArray = o.getJSONArray("product");

                            if (productJsonArray.length() > 0){
                                categoryModel.productList = new ArrayList<>();
                                for (int b = 0; b < productJsonArray.length(); b++){
                                    ProductModel productModel = new ProductModel();
                                    JSONObject oo = productJsonArray.getJSONObject(b);
                                    productModel.productId = oo.getInt("productId");
                                    productModel.catId = oo.getInt("catId");
                                    productModel.productUnitSizeCatId = oo.getInt("productUnitSizeCatId");
                                    productModel.productName = StaticClass.nullChecker(oo.getString("productName"));
                                    productModel.productDescription = StaticClass.nullChecker(oo.getString("productDescription"));
                                    productModel.productPrice = oo.getDouble("productPrice");
                                    productModel.productQuantityLeft = oo.getDouble("productQuantity");

                                    String pdc = StaticClass.nullChecker("productDiscount");

                                    if (pdc== null || pdc.isEmpty()){
                                        productModel.productDiscount = 0.0;
                                    }else {
                                        try {
                                            productModel.productDiscount = Double.parseDouble(pdc);
                                        }catch (Exception e){
                                            productModel.productDiscount = 0.0;
                                        }
                                    }

                                    productModel.productDiscount = oo.getDouble("productDiscount");
                                    productModel.productImage = StaticClass.nullChecker(oo.getString("productImage"));

                                    JSONArray measArray = oo.getJSONArray("measuringUnits");

                                    if (measArray.length() > 0) {
                                        productModel.measuringList = new ArrayList<>();
                                        for (int c = 0; c < measArray.length(); c++) {
                                            MeasuringUnitModel measuringUnitModel = new MeasuringUnitModel();
                                            JSONObject ooo = measArray.getJSONObject(c);
                                            measuringUnitModel.measCatId = ooo.getInt("measCatId");
                                            measuringUnitModel.measCatValues = ooo.getDouble("measCatValues");
                                            measuringUnitModel.measCatName = ooo.getString("measCatName");
                                            productModel.measuringList.add(measuringUnitModel);
                                        }
                                    }
                                    categoryModel.productList.add(productModel);
                                }
                            }else {
                                categoryModel.productList = new ArrayList<>();
                            }
                            categoryModels.add(categoryModel);
                        }
                        callback.onSuccess(categoryModels);
                    }else {
                        callback.onSuccess(new ArrayList<CategoryModel>());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {
                    if (error.networkResponse.statusCode == 301){

                        String s = error.networkResponse.headers.get("Location");

//                        getCategoryList(s, new ISC_CatList() {
//                            @Override
//                            public void onSuccess(ArrayList<CategoryModel> categoryList) {
//                                callback.onSuccess(categoryList);
//                            }
//
//                            @Override
//                            public void onError(String error) {
//                                callback.onError(error);
//                            }
//                        });

                        callback.onError(error.getLocalizedMessage());
                    }
                }catch (Exception e){
                    callback.onError("");
                }

//                callback.onError(error.getMessage());
            }
        }) ;

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    public void placeOrder(final OrderModel orderModel, final ISC_StringString callback){
        url = path + "order.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String msg = jsonObject.getString("msg");
                    String orderId = jsonObject.getString("orderId");

                    if (msg.equalsIgnoreCase("s")){
                        callback.onSuccess(orderId, msg);
                    }else if (msg.equalsIgnoreCase("f")){
                        callback.onError("Please Try again...");
                    }else {
                        callback.onError(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("productIdList", orderModel.productIdList);
                params.put("ProductNameList", orderModel.ProductNameList);
                params.put("productQtyList", orderModel.productQtyList);
                params.put("productQtyList2", orderModel.productQtyList2);
                params.put("orderType", orderModel.orderType);
                params.put("paymentType", orderModel.paymentType);
                params.put("OrderAmount", Double.toString(orderModel.OrderAmount));
                params.put("orderStatus", ConstantModel.pending);
                params.put("userId", Integer.toString(MySharedPref.getUserId(context)));
                params.put("userName", MySharedPref.getName(context));
                params.put("userPhone", MySharedPref.getMobileNo(context));
                params.put("userAddress", MySharedPref.getAddress(context));

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void isProductAvailable(final  int productId, final String productQty, final ISC_Boolean callback){
        url = path + "checkProducts.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");

                    if (status.equalsIgnoreCase("s")){
                        callback.onSuccess(true);
                    }else if (status.equalsIgnoreCase("f")){
                        callback.onSuccess(false);
                    }else {
                        callback.onError(status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("productId", String.valueOf(productId));
                params.put("productQuantity", productQty);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void changeOrderStatus(final  int orderId, final String status, final String reason, final ISC_String callback){
        url = path + "changeDeliveryStatus.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("msg");

                    if (status.equalsIgnoreCase("s")){
                        callback.onSuccess("s");
                    }else if (status.equalsIgnoreCase("f")){
                        callback.onSuccess("f");
                    }else {
                        callback.onError(status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("orderId", String.valueOf(orderId));
                params.put("orderStatus", status);
                params.put("inputReason", reason);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void getPendingOrder(final ISC_OrderList callback)   {
        url = path + "pending-order.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);

                    if (jsonArray.length() > 0){
                        ArrayList<OrderModel> orderModels = new ArrayList<>();

                        for (int a = 0; a < jsonArray.length(); a++){
                            JSONObject o = jsonArray.getJSONObject(a);
                            OrderModel orderModel = new OrderModel();

                            orderModel.orderId = o.getInt("orderId");
                            orderModel.productIdList = StaticClass.nullChecker(o.getString("productIdList"));
                            orderModel.ProductNameList = StaticClass.nullChecker(o.getString("ProductNameList"));
                            orderModel.productQtyList = StaticClass.nullChecker(o.getString("productQtyList"));
                            orderModel.orderType = StaticClass.nullChecker(o.getString("orderType"));
                            orderModel.paymentType = StaticClass.nullChecker(o.getString("paymentType"));
                            orderModel.orderStatus = StaticClass.nullChecker(o.getString("orderStatus"));
                            orderModel.userId = o.getInt("userId");
                            orderModel.userName = StaticClass.nullChecker(o.getString("userName"));
                            orderModel.userPhone = StaticClass.nullChecker(o.getString("userPhone"));
                            orderModel.userAddress = StaticClass.nullChecker(o.getString("userAddress"));
                            orderModel.userPinCode = StaticClass.nullChecker(o.getString("userPinCode"));
                            orderModel.OrderAmount = o.getDouble("OrderAmount");
                            orderModel.orderDate = StaticClass.nullChecker(o.getString("orderDate"));
                            orderModels.add(orderModel);
                        }

                        callback.onSuccess(orderModels);
                    }else {
                        callback.onSuccess(new ArrayList<OrderModel>());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userId",Integer.toString(MySharedPref.getUserId(context)));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void getOrder(final ISC_OrderList callback)   {
        url = path + "OrderList.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);

                    if (jsonArray.length() > 0){
                        ArrayList<OrderModel> orderModels = new ArrayList<>();

                        for (int a = 0; a < jsonArray.length(); a++){
                            JSONObject o = jsonArray.getJSONObject(a);
                            OrderModel orderModel = new OrderModel();

                            orderModel.orderId = o.getInt("orderId");
                            orderModel.productIdList = StaticClass.nullChecker(o.getString("productIdList"));
                            orderModel.ProductNameList = StaticClass.nullChecker(o.getString("ProductNameList"));
                            orderModel.productQtyList = StaticClass.nullChecker(o.getString("productQtyList"));
                            orderModel.orderType = StaticClass.nullChecker(o.getString("orderType"));
                            orderModel.paymentType = StaticClass.nullChecker(o.getString("paymentType"));
                            orderModel.orderStatus = StaticClass.nullChecker(o.getString("orderStatus"));
                            orderModel.userId = o.getInt("userId");
                            orderModel.userName = StaticClass.nullChecker(o.getString("userName"));
                            orderModel.userPhone = StaticClass.nullChecker(o.getString("userPhone"));
                            orderModel.userAddress = StaticClass.nullChecker(o.getString("userAddress"));
                            orderModel.userPinCode = StaticClass.nullChecker(o.getString("userPinCode"));
                            orderModel.OrderAmount = o.getDouble("OrderAmount");
                            orderModel.orderDate = StaticClass.nullChecker(o.getString("orderDate"));
                            orderModel.cancelReason = StaticClass.nullChecker(o.getString("cancellation_reason"));
                            orderModels.add(orderModel);
                        }

                        callback.onSuccess(orderModels);
                    }else {
                        callback.onSuccess(new ArrayList<OrderModel>());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userId",Integer.toString(MySharedPref.getUserId(context)));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void getDeliveredOrder(final ISC_OrderList callback)   {
        url = path + "delivered-order.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);

                    if (jsonArray.length() > 0){
                        ArrayList<OrderModel> orderModels = new ArrayList<>();

                        for (int a = 0; a < jsonArray.length(); a++){
                            JSONObject o = jsonArray.getJSONObject(a);
                            OrderModel orderModel = new OrderModel();

                            orderModel.orderId = o.getInt("orderId");
                            orderModel.productIdList = StaticClass.nullChecker(o.getString("productIdList"));
                            orderModel.ProductNameList = StaticClass.nullChecker(o.getString("ProductNameList"));
                            orderModel.productQtyList = StaticClass.nullChecker(o.getString("productQtyList"));
                            orderModel.orderType = StaticClass.nullChecker(o.getString("orderType"));
                            orderModel.paymentType = StaticClass.nullChecker(o.getString("paymentType"));
                            orderModel.orderStatus = StaticClass.nullChecker(o.getString("orderStatus"));
                            orderModel.userId = o.getInt("userId");
                            orderModel.userName = StaticClass.nullChecker(o.getString("userName"));
                            orderModel.userPhone = StaticClass.nullChecker(o.getString("userPhone"));
                            orderModel.userAddress = StaticClass.nullChecker(o.getString("userAddress"));
                            orderModel.orderDate = StaticClass.nullChecker(o.getString("orderDate"));
                            orderModel.userPinCode = StaticClass.nullChecker(o.getString("userPinCode"));
                            orderModel.OrderAmount = o.getDouble("OrderAmount");
                            orderModels.add(orderModel);
                        }

                        callback.onSuccess(orderModels);
                    }else {
                        callback.onSuccess(new ArrayList<OrderModel>());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userId",Integer.toString(MySharedPref.getUserId(context)));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
