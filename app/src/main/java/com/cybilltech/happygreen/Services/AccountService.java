package com.cybilltech.happygreen.Services;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cybilltech.happygreen.Callbacks.ISC_String;
import com.cybilltech.happygreen.Callbacks.ISC_UserObject;
import com.cybilltech.happygreen.Callbacks.ISC_OTPModel;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.OTPModel;
import com.cybilltech.happygreen.Models.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;

public class AccountService {

    private Context context;
    private RequestQueue requestQueue;
    private String path = "";
    private String url = "";

    public AccountService(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
        path = StaticClass.BASE_URL + "login_register/";
    }

    public void login(final String mobilNo, final String password, final ISC_UserObject callback) {
        url = path + "login.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String status = StaticClass.nullChecker(jsonObject.getString("status"));

                    if (StaticClass.nullChecker(status).equalsIgnoreCase("s")) {
                        UserModel useMOd = new UserModel();
                        JSONObject o = jsonObject.getJSONObject("user");
                        useMOd.userId = o.getInt("userId");
                        useMOd.first_name = StaticClass.nullChecker(o.getString("first_name"));
                        useMOd.last_name = StaticClass.nullChecker(o.getString("last_name"));
                        useMOd.phone = StaticClass.nullChecker(o.getString("phone"));
                        useMOd.email = StaticClass.nullChecker(o.getString("email"));
                        useMOd.address = StaticClass.nullChecker(o.getString("address"));
                        useMOd.pincode = StaticClass.nullChecker(o.getString("pincode"));
                        callback.onSuccess(useMOd);
                    } else {
                        callback.onError(StaticClass.nullChecker(jsonObject.getString("msg")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", StaticClass.nullChecker(mobilNo));
                params.put("password", StaticClass.nullChecker(password));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void signUp(final UserModel userModel, final ISC_UserObject callback) {

        url = path + "signup.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String status = StaticClass.nullChecker(jsonObject.getString("status"));

                    if (StaticClass.nullChecker(status).equalsIgnoreCase("s")) {
                        UserModel useMOd = new UserModel();
                        JSONObject o = jsonObject.getJSONObject("user");
                        useMOd.userId = o.getInt("userId");
                        useMOd.first_name = StaticClass.nullChecker(o.getString("first_name"));
                        useMOd.last_name = StaticClass.nullChecker(o.getString("last_name"));
                        useMOd.email = StaticClass.nullChecker(o.getString("email"));
                        useMOd.phone = StaticClass.nullChecker(o.getString("phone"));
                        callback.onSuccess(useMOd);
                    } else {
                        callback.onError("User Already Registered");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name", StaticClass.nullChecker(userModel.first_name));
                params.put("last_name", StaticClass.nullChecker(userModel.last_name));
                params.put("phone", StaticClass.nullChecker(userModel.phone));
                params.put("email", StaticClass.nullChecker(userModel.email));
                params.put("password", StaticClass.nullChecker(userModel.password));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void sendOtp(final String mobilNo, final ISC_OTPModel callback) {

        url = path + "forget.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println(response+"out side if");
                    if (response.length() > 10){
                        System.out.println("inside in if" );
                        String msg = StaticClass.nullChecker(jsonObject.getString("msg"));

                        if (StaticClass.nullChecker(msg).equalsIgnoreCase("OtpSent")) {
System.out.println("inside if 2");
                            OTPModel otpModel = new OTPModel();
                            otpModel.msg = StaticClass.nullChecker(jsonObject.getString("msg"));
                            otpModel.otp = StaticClass.nullChecker(jsonObject.getString("otp"));
                            otpModel.userId = jsonObject.getInt("userId");

                            callback.onSuccess(otpModel);
                        } else {
                            callback.onError(StaticClass.nullChecker(jsonObject.getString("msg")));
                        }
                    }else {
                        callback.onError("Mobile No not register.!!!");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", StaticClass.nullChecker(mobilNo));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 48,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void setPassword(final int userId, final String password, final ISC_UserObject callback){
        url = path + "set-password.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String status = StaticClass.nullChecker(jsonObject.getString("status"));

                    if (StaticClass.nullChecker(status).equalsIgnoreCase("s")) {
                        UserModel useMOd = new UserModel();
                        JSONObject o = jsonObject.getJSONObject("user");
                        useMOd.userId = o.getInt("userId");
                        useMOd.first_name = StaticClass.nullChecker(o.getString("first_name"));
                        useMOd.last_name = StaticClass.nullChecker(o.getString("last_name"));
                        useMOd.email = StaticClass.nullChecker(o.getString("email"));
                        useMOd.phone = StaticClass.nullChecker(o.getString("phone"));
                        callback.onSuccess(useMOd);
                    } else if (status.equals("f")){
                        callback.onError("Password change unsuccessful!!, Please try again...");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("change", "change");
                params.put("userId", Integer.toString(userId));
                params.put("password", StaticClass.nullChecker(password));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void updateAddress(final String email, final String address, final ISC_String callback){

        url = path + "update-addr.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (response.length() > 10){
                        String msg = StaticClass.nullChecker(jsonObject.getString("status"));

                        if (msg.equalsIgnoreCase("s") || msg.equalsIgnoreCase("f")){
                            callback.onSuccess(msg);
                        }else {
                            callback.onError(msg);
                        }

                    }else {
                        callback.onError("Something went wrong. PLease try again!!!");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userId", Integer.toString(MySharedPref.getUserId(context)));
                params.put("userEmail", email);
                params.put("userAddress", address);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 48,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}
