package com.cybilltech.happygreen.Common;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.CursorLoader;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class StaticClass {

    public static String BASE_URL = "https://happygreens.co.in/api/";

    public static String nullChecker(String string) {
        String returnString = "";
        if (string != null && !string.isEmpty() && !string.contains("null")) {
            returnString = string;
        }
        return returnString;
    }

    public static String DateFormetter(String inputFormat, String outputFormat, String datesToConvert) {

        String dateToReturn = datesToConvert;

        SimpleDateFormat sdf = new SimpleDateFormat(inputFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date gmt = null;

        SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat(outputFormat);
        sdfOutPutToSend.setTimeZone(TimeZone.getDefault());

        try {

            gmt = sdf.parse(datesToConvert);
            dateToReturn = sdfOutPutToSend.format(gmt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateToReturn;
//        if (!(DD_MM_YYYY == null || DD_MM_YYYY.equals("null") || DD_MM_YYYY.isEmpty()  || DD_MM_YYYY.equalsIgnoreCase("1900-01-01T00:00:00"))){
//            Date dt = null;
//
//            long TimeInMilis = 0;
//            try {
//                SimpleDateFormat dateFormat = new SimpleDateFormat(inputFormat, Locale.ENGLISH);
//                dt = dateFormat.parse(DD_MM_YYYY);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            TimeInMilis = dt.getTime();
//
//            Calendar calender = Calendar.getInstance();
//            calender.setTimeInMillis(TimeInMilis);
//
//            final String dateTimeFormatString = outputFormat;
//            String returnDate = DateFormat.format(dateTimeFormatString, calender).toString();
//            return returnDate;
//        } else {
//            return "";
//        }

    }
    public static String TimeFormat(String time){

        return "";
    }

    public static void showSnackToast(View view, String msg){
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
        TextView tv = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            tv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        snackbar.show();
    }

    public static void setListViewExpanded(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);

            if (view != null){

                if (i == 0)
                    view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

                view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += view.getMeasuredHeight();
            }
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

}
