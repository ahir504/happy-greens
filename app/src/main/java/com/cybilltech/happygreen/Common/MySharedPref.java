package com.cybilltech.happygreen.Common;


import android.content.Context;
import android.content.SharedPreferences;

import com.cybilltech.happygreen.Models.ProductModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MySharedPref {

    private static SharedPreferences sharedPreferences;
    private static String DATABASE = "My_prefs_Cybill+Ecoom";

    public static boolean getLoginStatus(Context context){
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("LoginStatus"+ DATABASE, false);
    }

    public static void setLoginStatus(Context context, boolean status){
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean("LoginStatus" + DATABASE, status).apply();
    }

    public static void clearAll(Context context){
        context.getSharedPreferences(DATABASE, 0).edit().clear().apply();
    }

    public static String getEmail(Context context){
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        return sharedPreferences.getString("email"+ DATABASE, "");
    }

    public static void setEmail(Context context, String email){
        if (email != null && !email.isEmpty()) {
            sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
            sharedPreferences.edit().putString("email" + DATABASE, email).apply();
        }
    }

    public static String getMobileNo(Context context){
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        return sharedPreferences.getString("MobileNo"+ DATABASE, "");
    }

    public static void setMobileNo(Context context, String MobileNo){
        if (MobileNo != null && !MobileNo.isEmpty()) {
            sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
            sharedPreferences.edit().putString("MobileNo" + DATABASE, MobileNo).apply();
        }
    }

    public static String getName(Context context){
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        return sharedPreferences.getString("Name"+ DATABASE, "");
    }

    public static void setName(Context context, String Name){
        if (Name != null && !Name.isEmpty()) {
            sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
            sharedPreferences.edit().putString("Name" + DATABASE, Name).apply();
        }
    }

    public static String getAddress(Context context){
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        return sharedPreferences.getString("Address"+ DATABASE, "");
    }

    public static void setAddress(Context context, String Address){
        if (Address != null && !Address.isEmpty()) {
            sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
            sharedPreferences.edit().putString("Address" + DATABASE, Address).apply();
        }
    }

    public static String getPinCode(Context context){
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        return sharedPreferences.getString("PinCode"+ DATABASE, "");
    }

    public static void setPinCode(Context context, String PinCode){
        if (PinCode != null && !PinCode.isEmpty()) {
            sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
            sharedPreferences.edit().putString("PinCode" + DATABASE, PinCode).apply();
        }
    }

    public static int getUserId(Context context){
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        return sharedPreferences.getInt("UserId"+ DATABASE, 0);
    }

    public static void setUserId(Context context, int UserId){
        if (UserId != 0 ) {
            sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
            sharedPreferences.edit().putInt("UserId" + DATABASE, UserId).apply();
        }
    }

    public static void setLoginId(Context context, String LoginId){
        if (LoginId != null && !LoginId.isEmpty()) {
            sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
            sharedPreferences.edit().putString("LoginId" + DATABASE, LoginId).apply();
        }
    }

    public static void addCartProduct(Context context, final ProductModel productModel) {
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        Gson gson = new Gson();
        ArrayList<ProductModel> arrayList = MySharedPref.getCartItems(context);
        arrayList.add(productModel);
        String json = gson.toJson(arrayList);
        sharedPreferences.edit().putString("CartItems", json).apply();
    }

    public static int getCartSize(Context context){
        int size = 0;

        ArrayList<ProductModel> arrayList = MySharedPref.getCartItems(context);

        if (arrayList == null){
            size = 0;
        }else {
            size = arrayList.size();
        }

        return size;

    }

    public static void addListCartItem(Context context, final ArrayList<ProductModel> arrayList) {
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = gson.toJson(arrayList);
        sharedPreferences.edit().putString("CartItems", json).apply();
    }

    public static void removeCartProduct(Context context, ProductModel productModel){

        ArrayList<ProductModel> arrayList = MySharedPref.getCartItems(context);
        for (int a = 0; a < arrayList.size(); a++){
            if (arrayList.get(a).productId == productModel.productId){
                arrayList.remove(a);
                break;
            }
        }

        addListCartItem(context, arrayList);
    }

    public static ArrayList<ProductModel> getCartItems(Context context) {
        sharedPreferences = context.getSharedPreferences(DATABASE, context.MODE_PRIVATE);
        ArrayList<ProductModel> productModelArrayList = new ArrayList<>();
        String json = sharedPreferences.getString("CartItems", "");
        if (json.length() > 0) {
            Type type = new TypeToken<ArrayList<ProductModel>>() {}.getType();
            productModelArrayList = new Gson().fromJson(json, type);
            if (productModelArrayList == null) {
                productModelArrayList = new ArrayList<>();
            }
        }else{
            productModelArrayList = new ArrayList<>();
        }
        return productModelArrayList;
    }

}

