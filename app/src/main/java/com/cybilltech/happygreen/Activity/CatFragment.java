package com.cybilltech.happygreen.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cybilltech.happygreen.Adapters.ProductAdapter;
import com.cybilltech.happygreen.Callbacks.ISC_Boolean;
import com.cybilltech.happygreen.Callbacks.ISC_Product_addCallback;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.ManageProductService;

import java.util.ArrayList;


public class CatFragment extends Fragment {


    private ProgressDialog progressDialog;
    private ListView listView;
    private ArrayList<ProductModel> productList;
    private String catName="";
    private Context context = getActivity();


    public CatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_cat, container, false);
        context = getActivity();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("");
        progressDialog.setMessage("Please wait....");
        progressDialog.setCancelable(false);

        listView = view.findViewById(R.id.catFrah_listview);


        productList = (ArrayList<ProductModel>) getArguments().getSerializable("arrayList");
        catName = getArguments().getString("catName");

        if (productList != null && productList.size() > 0) {
            ProductAdapter adapter = new ProductAdapter(getActivity(), productList, new ISC_Product_addCallback() {
                @Override
                public void onClick(final ProductModel productModel, final double amount, final MeasuringUnitModel measuringUnitModel) {
                    progressDialog.show();
                    new ManageProductService(context).isProductAvailable(productModel.productId, String.format("%.2f", measuringUnitModel.measCatValues), new ISC_Boolean() {
                        @Override
                        public void onSuccess(boolean isAvailable) {
                            progressDialog.dismiss();
                            if(isAvailable){
                                ProductListActivity productListActivity = (ProductListActivity) getActivity();
                                productListActivity.addItemToCart(getActivity(), productModel, amount, measuringUnitModel);
                            }else {
                                CustomViews.alertDialogToast(context, "Selected quantity not available", ConstantModel.FailedAlertDialog);
                            }
                        }

                        @Override
                        public void onError(String error) {
                            progressDialog.dismiss();
                            CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
                        }
                    });
                }
            });

            listView.setAdapter(adapter);
        }


        return view;

    }

    public static CatFragment newInstance(String catName, ArrayList<ProductModel> arrayList) {
        CatFragment fragment = new CatFragment();
        Bundle args = new Bundle();
        args.putSerializable("arrayList", arrayList);
        args.putString("catName", catName);
        fragment.setArguments(args);
        return fragment;
    }

}
