package com.cybilltech.happygreen.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.cybilltech.happygreen.R;

public class ProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        TextView textView =findViewById(R.id.textView);
        String productname=getIntent().getStringExtra("productName");
        textView.setText(productname);
    }
}