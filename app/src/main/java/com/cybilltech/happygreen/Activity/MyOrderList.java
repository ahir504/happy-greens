package com.cybilltech.happygreen.Activity;



import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cybilltech.happygreen.Adapters.OrderBookAdapter;
import com.cybilltech.happygreen.Callbacks.ISC_AlertClickPositiveOnly;
import com.cybilltech.happygreen.Callbacks.ISC_OrderList;
import com.cybilltech.happygreen.Callbacks.ISC_String;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.OrderModel;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.ManageProductService;

import java.util.ArrayList;

public class MyOrderList extends AppCompatActivity {

    private final Context context = MyOrderList.this;
    private ProgressDialog progressDialog;
    private ListView listView;
    private ArrayList<OrderModel> arrayList = new ArrayList<>();
    private TextView tvNoData;
    String reason = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_list);
        init();

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                OrderModel orderModel = (OrderModel) parent.getItemAtPosition(position);

                if (orderModel.orderStatus.equalsIgnoreCase("Order Booked")){
                    showContextMenu(orderModel);
                }
                return false;
            }
        });
    }

    private void init() {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(StaticClass.nullChecker("My Order List"));
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.colorSecondary));

        listView = findViewById(R.id.myOrder_lv_orderList);
        tvNoData = findViewById(R.id.myOrder_tv_NoData);
        tvNoData.setVisibility(View.GONE);
        listView.setVisibility(View.GONE);
        loadList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra(ConstantModel.activityName)){
            if (getIntent().getStringExtra(ConstantModel.activityName).equalsIgnoreCase("paymentSuccesful")){
                startActivity(new Intent(context, HomeActivity.class));
                overridePendingTransition(R.anim.push_right_to_left_in, R.anim.push_right_to_left_out);
            }else {
                super.onBackPressed();
            }
        }else {
            super.onBackPressed();
        }
    }

    private void loadList(){
        progressDialog.show();
        arrayList = new ArrayList<>();
//        new ManageProductService(context).getPendingOrder(new ISC_OrderList() {
//            @Override
//            public void onSuccess(final ArrayList<OrderModel> orderListPending) {
//                progressDialog.dismiss();
//                new  ManageProductService(context).getDeliveredOrder(new ISC_OrderList() {
//                    @Override
//                    public void onSuccess(final ArrayList<OrderModel> orderListDelivered) {

                        new ManageProductService(context).getOrder(new ISC_OrderList() {
                            @Override
                            public void onSuccess(ArrayList<OrderModel> OrderList) {

//                                for ( OrderModel orderModel: orderListPending ) {
//                                    arrayList.add(orderModel);
//                                }
//
//
//                                for ( OrderModel orderModel: orderListDelivered ) {
//                                    arrayList.add(orderModel);
//                                }

                                for ( OrderModel orderModel: OrderList ) {
                                    arrayList.add(orderModel);
                                }
                                progressDialog.dismiss();
                                if (arrayList != null & arrayList.size() > 0) {
                                    listView.setVisibility(View.VISIBLE);
                                    tvNoData.setVisibility(View.GONE);
                                    OrderBookAdapter orderBookAdapter = new OrderBookAdapter(context, arrayList);
                                    listView.setAdapter(orderBookAdapter);


                                }else {
                                    tvNoData.setVisibility(View.VISIBLE);
                                    listView.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onError(String error) {
                                progressDialog.dismiss();
                                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
                            }
                        });
                    }

//                    @Override
//                    public void onError(String error) {
//                        progressDialog.dismiss();
//                        CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
//                    }
//                });
//            }
//
//            @Override
//            public void onError(String error) {
//                progressDialog.dismiss();
//                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
//            }
//        });
//    }

    private void changeOrderStatus(int orderId, final String status, final String reason){
        progressDialog.show();
        new ManageProductService(context).changeOrderStatus(orderId, status, reason, new ISC_String() {
            @Override
            public void onSuccess(String msg) {
                progressDialog.dismiss();

                if (msg.equalsIgnoreCase("s")){
                    CustomViews.alertDialogToast(context, "Order Status changed Successfully", ConstantModel.SuccessAlertDialog, new ISC_AlertClickPositiveOnly() {
                        @Override
                        public void onPositiveButtonClicked(View view) {
                            loadList();
                        }
                    });
                }else if (msg.equalsIgnoreCase("f")){
                    CustomViews.alertDialogToast(context, "Order Status not changed, Please try againg.!!!", ConstantModel.FailedAlertDialog);
                }
                else {
                    CustomViews.alertDialogToast(context, msg, ConstantModel.FailedAlertDialog);
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
            }
        });
    }

    private void showContextMenu(final OrderModel orderModel){
        final CharSequence[] items = {"Order Cancelled", "Dismiss"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Order Status");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              if (items[which].equals("Order Cancelled")){
                    String status = "Cancelled";
                    dialog.dismiss();
                    showAddReason(orderModel,status);

                }else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void showAddReason( final OrderModel orderModel,final String status){
//        AlertDialog.Builder builder =new AlertDialog.Builder(context);
//        builder.setCancelable(false);
//        View view=getLayoutInflater().inflate(R.layout.custom_cancel_reason,null);
//        builder.setView(view);
//        final AlertDialog alertDialog =builder.create();
//        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        alertDialog.show();
//
//
//        view.findViewById(R.id.cencel_group).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                radioButton=findViewById(radioGroup.getCheckedRadioButtonId());
//                reason =radioButton.getText().toString();
//                Toast.makeText(MyOrderList.this, ""+reason, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        view.findViewById(R.id.cancel_rrason).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                alertDialog.dismiss();
//            }
//        });
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyOrderList.this);
        alertDialog.setTitle("Select Cancel Reason");
        String[] items = {"Order by Mistake","Forget Something to Order","Order Something Extra","Find Better Deal","Change my Mind"};
        int checkedItem = 1;
        alertDialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        reason="Order by Mistake";
                        break;
                    case 1:
                        reason="Forget Something to order";
                        break;
                    case 2:
                        reason="Order Something extra";
                        break;
                    case 3:
                        reason="Find Better Deal";
                        break;
                    case 4:
                        reason="Change my mind";
                        break;
                }
            }
        });
        alertDialog.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                changeOrderStatus(orderModel.orderId, status, reason);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
        }


    }



