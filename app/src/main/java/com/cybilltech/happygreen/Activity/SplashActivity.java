package com.cybilltech.happygreen.Activity;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.R;

public class SplashActivity extends AppCompatActivity {

    private Context context = SplashActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        ImageView ivLogo = findViewById(R.id.splash_iv_logo);
        ImageView ivBackImage = findViewById(R.id.splash_iv_img_back);

        ivLogo.animate().alpha(0f);
        final Animation animation = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        ivBackImage.setAnimation(animation);

        final ObjectAnimator anim = ObjectAnimator.ofFloat(ivBackImage,"alpha",0f);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(ivLogo,"alpha",1f);
        anim.setDuration(3000);
        anim2.setDuration(2500);

        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(anim, anim2);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                animatorSet.start();
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (MySharedPref.getLoginStatus(context)) {
                            startActivity(new Intent(context, HomeActivity.class));
                            overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
                            finish();
                        } else {
                            startActivity(new Intent(context, LoginActivity.class));
                            overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
                            finish();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }, 500);


    }
}
