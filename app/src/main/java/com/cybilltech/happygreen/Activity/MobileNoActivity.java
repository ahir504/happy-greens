package com.cybilltech.happygreen;

/**
 * @auther Harish Ruhil
 * harishruhil98@gmail.com
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cybilltech.happygreen.Activity.LoginActivity;
import com.cybilltech.happygreen.Callbacks.ISC_AlertClickPositiveOnly;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.Callbacks.ISC_OTPModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.OTPModel;
import com.cybilltech.happygreen.Services.AccountService;

public class MobileNoActivity extends AppCompatActivity {

    private Context context = MobileNoActivity.this;
    private ProgressDialog progressDialog;
    private String _action = "";
    private EditText etMobileNo;
    private static final String forgetPass = "forgetPass";
    private TextView tvAction;
    private Button btnSendOtp;
    private ScrollView parentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_no);

        progressDialog =  new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        etMobileNo = findViewById(R.id.mobileNo_et_mobileNO);
        tvAction = findViewById(R.id.mobileNo_tv_action);
        btnSendOtp = findViewById(R.id.mobileNO_btn_SendOtp);
        parentLayout = findViewById(R.id.mobileNo_sv_parentLayout);


        try {
            _action = StaticClass.nullChecker(getIntent().getStringExtra(ConstantModel.action));
        }catch (Exception e){
            _action = "";
        }

        btnSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etMobileNo.getText().toString().trim().length() < 5){
                    etMobileNo.setError("Please Enter a valid mobile No");
                    CustomViews.alertDialogToast(context, "Please Enter a valid mobile No", ConstantModel.FailedAlertDialog);
                    return;
                }else if (_action.equalsIgnoreCase(forgetPass)){
                    progressDialog.show();
                    new AccountService(context).sendOtp(etMobileNo.getText().toString().trim(), new ISC_OTPModel() {
                        @Override
                        public void onSuccess(OTPModel otpModel) {
                            progressDialog.dismiss();
                            if (StaticClass.nullChecker(otpModel.otp).length() > 3 && StaticClass.nullChecker(otpModel.msg).equalsIgnoreCase("OtpSent") && otpModel.userId > 0){
                                CustomViews.alertDialogToast(context, "Password Reset link has been sent to your Email Id", ConstantModel.SuccessAlertDialog, new ISC_AlertClickPositiveOnly() {
                                    @Override
                                    public void onPositiveButtonClicked(View view) {
                                        Intent intent = new Intent(context, LoginActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.push_left_to_right_in, R.anim.push_left_to_right_out);
                                        finish();
                                    }
                                });

                            }else {
                                CustomViews.alertDialogToast(context, "Something went wrong, Please try again!!!", ConstantModel.FailedAlertDialog);
                            }
                        }

                        @Override
                        public void onError(String error) {
                            progressDialog.dismiss();
                            CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
                        }
                    });
                }
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
