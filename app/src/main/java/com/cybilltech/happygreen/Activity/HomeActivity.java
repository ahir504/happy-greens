package com.cybilltech.happygreen.Activity;



import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.TextView;

import com.android.volley.BuildConfig;
import com.cybilltech.happygreen.Adapters.CategoryListAdapter;
import com.cybilltech.happygreen.Adapters.DealAdapter;
import com.cybilltech.happygreen.Adapters.RvListAdapter;
import com.cybilltech.happygreen.Adapters.ViewPagerAdapter;
import com.cybilltech.happygreen.Callbacks.ISC_AlertClick;
import com.cybilltech.happygreen.Callbacks.ISC_CatList;
import com.cybilltech.happygreen.Callbacks.ISC_DealList;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.CustomClasses.CountDrawable;
import com.cybilltech.happygreen.Models.CategoryModel;
import com.cybilltech.happygreen.Models.DealModel;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.ManageProductService;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Context context = HomeActivity.this;
    private boolean doubleBackToExitPressedOnce = false;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout homeDrawer;
    private int currentPage = 0;
    private ScrollView parentLayout;
    private ArrayList<Integer> arrayList;
    private ListView lvCatList,rvDealList;
    private ProgressDialog progressDialog;
    private MenuItem menuItem;
    private ArrayList<CategoryModel> catList = new ArrayList<>();
    private ArrayList<DealModel> dealList = new ArrayList<>();
    private RecyclerView rvCatList;
    private ViewPager pager;
    ViewPagerAdapter pagerAdapter;
    Timer timer;

    TextView text;
    private  final int [] img={R.drawable.img_item_veg_2,
            R.drawable.img_item_veg_3,
            R.drawable.img_item_veg_5,
            R.drawable.img_item_veg_6,};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        
        init();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        menuItem = menu.findItem(R.id.menu_cart_cartItem);
        setCartCount(Integer.toString(MySharedPref.getCartSize(context)));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {

            case R.id.menu_cart_cartItem: {
                if (MySharedPref.getCartSize(context) > 0) {
                    startActivity(new Intent(context, OrderListActivity.class));
                } else {
                    CustomViews.alertDialogToast(context, "No Item in Cart", ConstantModel.FailedAlertDialog);
                }
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    private void setCartCount(String count) {
        try {


            CountDrawable badge;
            LayerDrawable icon = (LayerDrawable) menuItem.getIcon();

            // Reuse drawable if possible
            Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
            if (reuse != null && reuse instanceof CountDrawable) {
                badge = (CountDrawable) reuse;
            } else {
                badge = new CountDrawable(context);
            }

            badge.setCount(count);
            icon.mutate();
            icon.setDrawableByLayerId(R.id.ic_group_count, badge);
        } catch (Exception e) {
        }
    }

    private void init() {

        homeDrawer = findViewById(R.id.Navigationdrawer);
        toggle = new ActionBarDrawerToggle(HomeActivity.this, homeDrawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        homeDrawer.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.colorSecondary));
        getSupportActionBar().setTitle(R.string.app_name);

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");

        NavigationView navigationView = findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        lvCatList = findViewById(R.id.home_CatListView);
        rvCatList = findViewById(R.id.custm_cat_list);
        rvDealList = findViewById(R.id.custom_deal_list);



        pager=findViewById(R.id.sliderImageLayout);

        pagerAdapter =new ViewPagerAdapter(img);
        pager.setAdapter(pagerAdapter);

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                pager.post(new Runnable(){

                    @Override
                    public void run() {
                        pager.setCurrentItem((pager.getCurrentItem()+1)%img.length);
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask, 3000, 3000);


        parentLayout = findViewById(R.id.home_scrollView);
        parentLayout.setFocusable(true);

        navigationView.getMenu().findItem(R.id.nav_version).setTitle(" Version " + BuildConfig.VERSION_NAME);

        View headerView = navigationView.getHeaderView(0);
        navigationView.setItemIconTintList(null);

        TextView tvHeaderName = headerView.findViewById(R.id.tv_nav_header_home_name);
        TextView tvHeaderMobile = headerView.findViewById(R.id.tv_nav_header_home_mobileNo);
        TextView tvHeaderEmail = headerView.findViewById(R.id.tv_nav_header_home_email);

        tvHeaderName.setText(StaticClass.nullChecker(MySharedPref.getName(context)).toUpperCase());
        tvHeaderMobile.setText(StaticClass.nullChecker(MySharedPref.getMobileNo(context)));
        tvHeaderEmail.setText(StaticClass.nullChecker(MySharedPref.getEmail(context)));

        //loadViewPager();
        loadCatList();
        loadDealList();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                parentLayout.scrollTo(0, parentLayout.getTop());
            }
        }, 50);

        LinearLayout llStandardLayout = findViewById(R.id.home_statndardOrder);
        LinearLayout llAdvanceLayout = findViewById(R.id.home_super);

        llStandardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (catList != null && catList.size() > 0) {
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.putExtra(ConstantModel.action, "ProductList");
                    intent.putExtra("categoryList", catList);
                    intent.putExtra("position", 0);
                    startActivity(intent);
                } else {
                    CustomViews.alertDialogToast(context, "", ConstantModel.FailedAlertDialog);
                }
            }
        });

        llAdvanceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (catList != null && catList.size() > 0) {
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.putExtra(ConstantModel.action, "ProductList");
                    intent.putExtra("categoryList", catList);
                    intent.putExtra("position", 0);
                    startActivity(intent);
                } else {
                    CustomViews.alertDialogToast(context, "", ConstantModel.FailedAlertDialog);
                }
            }
        });
    }
    private void loadDealList(){

        progressDialog.show();
        new ManageProductService(context).getDealList(new ISC_DealList(){

            @Override
            public void onSuccess(ArrayList<DealModel> dealModels) {
                progressDialog.dismiss();
                dealList =dealModels;
                DealAdapter dealAdapter=new DealAdapter(context,dealModels);
                rvDealList.setAdapter(dealAdapter);
                StaticClass.setListViewExpanded(rvDealList);

            }

            @Override
            public void onError(String error) {

                progressDialog.dismiss();
                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
            }
        });
    }

    private void loadCatList() {

        progressDialog.show();
        new ManageProductService(context).getCategoryList(new ISC_CatList() {
            @Override
            public void onSuccess(ArrayList<CategoryModel> categoryList) {
                progressDialog.dismiss();
                catList = categoryList;
                CategoryListAdapter categoryListAdapter = new CategoryListAdapter(context, categoryList);
                RvListAdapter rvListAdapter=new RvListAdapter(context,categoryList);
                lvCatList.setAdapter(categoryListAdapter);
                StaticClass.setListViewExpanded(lvCatList);
                rvCatList.setAdapter(rvListAdapter);
                rvCatList.setHasFixedSize(true);
                rvCatList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;

        StaticClass.showSnackToast(parentLayout, "Please press BACK again to exit!!!");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.nav_home_item_vegetable: {
                catClickFunction("Vegetable");
                break;
            }

            case R.id.nav_home_item_fruits: {
                catClickFunction("Fruits");
                break;
            }

            case R.id.nav_home_item_seasonal_items: {
                startActivity(new Intent(HomeActivity.this,Contact.class));
                break;
            }

            case R.id.nav_home_item_cart: {
                if (MySharedPref.getCartSize(context) > 0) {
                    startActivity(new Intent(context, OrderListActivity.class));
                } else {
                    CustomViews.alertDialogToast(context, "Cart is empty.", ConstantModel.FailedAlertDialog);
                }
                break;
            }

            case R.id.nav_home_item_order_book: {
                startActivity(new Intent(context, MyOrderList.class));
                break;
            }

            case R.id.nav_home_item_user_profile: {
                startActivity(new Intent(context, ProfileActivity.class));
                break;
            }

            case R.id.nav_home_item_feedback: {
                startActivity(new Intent(context, FeedbackActivity.class));
                break;
            }

            case R.id.nav_home_item_about_us: {
                startActivity(new Intent(context, AboutUSActivity.class));
                break;
            }

            case R.id.nav_home_item_logout: {
                logoutFunction();
                break;
            }

        }
        homeDrawer = findViewById(R.id.Navigationdrawer);
        homeDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logoutFunction() {
        CustomViews.alertDialogToast(context, "Are you sure want to logout", ConstantModel.LogoutAlertDialog, new ISC_AlertClick() {
            @Override
            public void onPositiveButtonClicked(View view) {
                MySharedPref.clearAll(context);
                startActivity(new Intent(context, LoginActivity.class));
                overridePendingTransition(R.anim.push_right_to_left_in, R.anim.push_right_to_left_out);
                finish();
            }

            @Override
            public void onNegativeButtonClicked(View view) {

            }
        });
    }

//    private void loadViewPager() {
//        View headerView = LayoutInflater.from(context).inflate(R.layout.custom_logo_view, null, false);
//        final ViewPager viewPager = headerView.findViewById(R.id.viewPager);
//
//        arrayList = new ArrayList();
//        arrayList.add(R.drawable.img_item_veg_2);
//        arrayList.add(R.drawable.img_item_veg_3);
//        //arrayList.add(R.drawable.img_item_veg_4);
//        arrayList.add(R.drawable.img_item_veg_5);
//        arrayList.add(R.drawable.img_item_veg_6);
////        arrayList.add(R.drawable.img_item_veg_7);
////        arrayList.add(R.drawable.img_item_veg_8);
////        arrayList.add(R.drawable.img_item_veg_9);
////        arrayList.add(R.drawable.img_item_veg_10);
//
//
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(arrayList, context);
//        viewPager.setAdapter(viewPagerAdapter);
//
//        viewPager = findViewById(R.id.sliderImageLayout);
//        viewPager.addView(headerView);
//
//        try {
//            Field mScroller;
//            mScroller = ViewPager.class.getDeclaredField("mScroller");
//            mScroller.setAccessible(true);
//            FixedSpeedScroller scroller = new FixedSpeedScroller(viewPager.getContext());
//            mScroller.set(viewPager, scroller);
//        } catch (Exception e) {
//            CustomViews.alertDialogToast(context, e.getLocalizedMessage(), ConstantModel.FailedAlertDialog);
//        }
//
//        final Handler handler = new Handler();
//        final Runnable Update = new Runnable() {
//            public void run() {
//                if (currentPage == arrayList.size()) {
//                    currentPage = 0;
//                }
//                viewPager.setCurrentItem(currentPage++, true);
//            }
//        };
//        Timer timer = new Timer();
//        //delay in milliseconds before task is to be executed
//        long DELAY_MS = 300;
//        // time in milliseconds between successive task executions.
//        long PERIOD_MS = 1500;
//        timer.schedule(new TimerTask() { // task to be scheduled
//
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, DELAY_MS, PERIOD_MS);
//
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            }
//
//            @Override
//            public void onPageSelected(int positionSelected) {
//                currentPage = positionSelected;
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//            }
//        });
//
//        viewPager.setFocusable(true);
//    }

//    public class FixedSpeedScroller extends Scroller {
//
//        private int mDuration = 2500;
//
//        public FixedSpeedScroller(Context context) {
//            super(context);
//        }
//
//        public FixedSpeedScroller(Context context, Interpolator interpolator) {
//            super(context, interpolator);
//        }
//
//        public FixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
//            super(context, interpolator, flywheel);
//        }
//
//
//        @Override
//        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
//            // Ignore received duration, use fixed one instead
//            super.startScroll(startX, startY, dx, dy, mDuration);
//        }
//
//        @Override
//        public void startScroll(int startX, int startY, int dx, int dy) {
//            // Ignore received duration, use fixed one instead
//            super.startScroll(startX, startY, dx, dy, mDuration);
//        }
//    }

    private void catClickFunction(String catName) {

        for (int a = 0; a < catList.size(); a++) {

            if (catList.get(a).catName.contains(catName)) {
                CategoryModel categoryModel = catList.get(a);
                if (categoryModel.productList != null && categoryModel.productList.size() > 0) {
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.putExtra(ConstantModel.action, "ProductList");
                    intent.putExtra("categoryList", catList);
                    intent.putExtra("position", a);
                    context.startActivity(intent);
                    break;
                } else {
                    CustomViews.alertDialogToast(context, "No items in this category", ConstantModel.FailedAlertDialog);
                    break;
                }
            }
        }

    }

    @Override
    protected void onResume() {
        loadCatList();
        loadDealList();
        setCartCount(Integer.toString(MySharedPref.getCartSize(context)));
        super.onResume();
    }
}
