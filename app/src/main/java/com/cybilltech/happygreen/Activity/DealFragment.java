package com.cybilltech.happygreen.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cybilltech.happygreen.Adapters.DealProductAdapter;
import com.cybilltech.happygreen.Callbacks.ISC_Boolean;
import com.cybilltech.happygreen.Callbacks.ISC_Product_addCallback;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.ManageProductService;

import java.util.ArrayList;

public class DealFragment extends Fragment {

    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private ArrayList<ProductModel> productList;
    private String dealName="";
    private Context context = getActivity();

    public DealFragment (){
   }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_deal_product_list, container, false);
        context = getActivity();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("");
        progressDialog.setMessage("Please wait....");
        progressDialog.setCancelable(false);

        recyclerView = view.findViewById(R.id.deal_productList);


        productList = (ArrayList<ProductModel>) getArguments().getSerializable("arrayList");
        dealName = getArguments().getString("catName");

        if (productList != null && productList.size() > 0) {
            DealProductAdapter adapter = new DealProductAdapter(getActivity(), productList, new ISC_Product_addCallback() {
                @Override
                public void onClick(final ProductModel productModel, final double amount, final MeasuringUnitModel measuringUnitModel) {
                    progressDialog.show();
                    new ManageProductService(context).isProductAvailable(productModel.productId, String.format("%.2f", measuringUnitModel.measCatValues), new ISC_Boolean() {
                        @Override
                        public void onSuccess(boolean isAvailable) {
                            progressDialog.dismiss();
                            if(isAvailable){
                                DealProductList productListActivity = (DealProductList) getActivity();
                                productListActivity.addItemToCart(getActivity(), productModel, amount, measuringUnitModel);
                            }else {
                                CustomViews.alertDialogToast(context, "Selected quantity not available", ConstantModel.FailedAlertDialog);
                            }
                        }

                        @Override
                        public void onError(String error) {
                            progressDialog.dismiss();
                            CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
                        }
                    });
                }
            });

            recyclerView.setAdapter(adapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        }


        return view;

    }

    public static DealFragment newInstance(String dealName, ArrayList<ProductModel> arrayList) {
        DealFragment fragment = new DealFragment();
        Bundle args = new Bundle();
        args.putSerializable("arrayList", arrayList);
        args.putString("dealName", dealName);
        fragment.setArguments(args);
        return fragment;
    }
}
