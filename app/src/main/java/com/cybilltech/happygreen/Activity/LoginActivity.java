package com.cybilltech.happygreen.Activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_UserObject;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.MobileNoActivity;
import com.cybilltech.happygreen.Models.UserModel;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.AccountService;

public class LoginActivity extends AppCompatActivity {

    private Context context=LoginActivity.this;
    private EditText etLoginId, etPass;
    private TextView tvBtnSignUp;
    private Button cvBtnLogin;
    private ScrollView parentLayout;
    private boolean doubleBackToExitPressedOnce = false;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        init();

        cvBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etLoginId.getText().toString().trim().length() != 10){
                    etLoginId.setError("Please enter a valid mobile no.");
                    return;
                }else if (etPass.getText().toString().trim().length() < 3){
                    etPass.setError("Please enter minimum four letters");
                    return;
                }else {

                    progressDialog.show();
                    new AccountService(context).login(etLoginId.getText().toString().trim(), etPass.getText().toString().trim(), new ISC_UserObject() {
                        @Override
                        public void onSuccess(UserModel userModel) {
                            progressDialog.dismiss();
                            if (userModel.userId > 0) {

                                MySharedPref.setLoginStatus(context, true);
                                MySharedPref.setName(context, userModel.first_name +" "+userModel.last_name);
                                MySharedPref.setEmail(context, userModel.email);
                                MySharedPref.setUserId(context, userModel.userId);
                                MySharedPref.setMobileNo(context, userModel.phone);
                                MySharedPref.setLoginId(context, userModel.phone);
                                MySharedPref.setAddress(context, StaticClass.nullChecker(userModel.address));
                                MySharedPref.setPinCode(context, StaticClass.nullChecker(userModel.pincode));

                                startActivity(new Intent(context, HomeActivity.class));
                                overridePendingTransition(R.anim.push_right_to_left_in, R.anim.push_right_to_left_out);
                                finish();
                            } else {
                                CustomViews.alertDialogToast(context, "Something went wrong, Please Try again...", ConstantModel.FailedAlertDialog);
                            }
                        }

                        @Override
                        public void onError(String error) {
                            progressDialog.dismiss();
                            if (StaticClass.nullChecker(error).length() == 0) {
                                CustomViews.alertDialogToast(context, "Your internet connection is not working. Please check your connectivity.!!!", ConstantModel.FailedAlertDialog);
                            } else {

                                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
                            }

                        }
                    });
                }
            }
        });

        TextView tvForgetPass = findViewById(R.id.login_tv_ForgotPass);
        tvForgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MobileNoActivity.class).putExtra(ConstantModel.action, "forgetPass"));
                overridePendingTransition(R.anim.push_left_to_right_in, R.anim.push_left_to_right_out);
                finish();
            }
        });

        tvBtnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SignUpActivity.class));
                overridePendingTransition(R.anim.push_left_to_right_in, R.anim.push_left_to_right_out);
                finish();
            }
        });
    }

    private void init() {
        etLoginId = findViewById(R.id.login_et_loginId);
        etPass = findViewById(R.id.login_et_loginPass);
        cvBtnLogin = findViewById(R.id.login_tv_loginBtn);
        tvBtnSignUp = findViewById(R.id.login_tv_signUp);
        parentLayout = findViewById(R.id.login_sv_parentLayout);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        StaticClass.showSnackToast(parentLayout, "Please press BACK again to exit!!!");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


}