package com.cybilltech.happygreen.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cybilltech.happygreen.Callbacks.ISC_AlertClickPositiveOnly;
import com.cybilltech.happygreen.Callbacks.ISC_String;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.CommonService;

public class FeedbackActivity extends AppCompatActivity {

    private Context context = FeedbackActivity.this;
    private ProgressDialog progressDialog;
    private EditText etFeedback;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        init();
    }

    private void init() {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(StaticClass.nullChecker("Feedback"));
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.colorSecondary));

        etFeedback = findViewById(R.id.feedback_et_feedback);
        btnSubmit = findViewById(R.id.feedback_btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etFeedback.getText().toString().trim().length() > 3){
                    saveFeedback(etFeedback.getText().toString());
                }else {
                    etFeedback.setError("Enter a valid feedback !!!");
                }
            }
        });

    }

    private void saveFeedback(String feedback) {
        progressDialog.show();
        new CommonService(context).saveFeedback(feedback, new ISC_String() {
            @Override
            public void onSuccess(String msg) {
                progressDialog.dismiss();
                if (msg.equalsIgnoreCase("s")){
                    etFeedback.setText("");
                    CustomViews.alertDialogToast(context, "Feedback Added Successfully", ConstantModel.SuccessAlertDialog, new ISC_AlertClickPositiveOnly() {
                        @Override
                        public void onPositiveButtonClicked(View view) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onBackPressed();
                                }
                            }, 2000);
                        }
                    });
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
