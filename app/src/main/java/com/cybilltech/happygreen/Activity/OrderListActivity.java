package com.cybilltech.happygreen.Activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.cybilltech.happygreen.Adapters.OrderAdapter;
import com.cybilltech.happygreen.Callbacks.ISC_Click;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class OrderListActivity extends AppCompatActivity {

    private Context context = OrderListActivity.this;
    private ProgressDialog progressDialog;
    private ListView listview;
    private Button btnCheckout;
    private double totalCost=0f;
    private ArrayList<ProductModel> productList = new ArrayList<>();
    private ArrayList<ProductModel> arrayList;
    private Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        productList = MySharedPref.getCartItems(context);
        init();

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    String orderType = "";

                    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss", Locale.UK);

                    String fromTime = "06:00:00";
                    String timeTo = "23:59:59";
                    String currentTime = formatter.format(calendar.getTime());

                    try {
                        Date dateFrom = formatter.parse(fromTime);
                        Date dateto = formatter.parse(timeTo);
                        Date dateCurrent = formatter.parse(currentTime);


                        if (dateFrom.before(dateCurrent) && dateto.after(dateCurrent)) {
                            startActivity(new Intent(context, PaymetActivity.class));
                        }
                        else {
                            CustomViews.alertDialogToast(context, "You Can Order Between 6:00 AM TO 12:00 PM", ConstantModel.FailedAlertDialog);

                        }


                    } catch (Exception e) {
                        orderType = ConstantModel.unknownOrder;
                    }

                }



        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    private void init() {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Order List");
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.colorSecondary));

        listview = findViewById(R.id.orderList_lv_orderList);
        btnCheckout = findViewById(R.id.orderList_btn_checkOut);

        arrayList = MySharedPref.getCartItems(context);
        OrderAdapter adapter = new OrderAdapter(context, arrayList, new ISC_Click() {
            @Override
            public void onClick(View view) {
                arrayList = MySharedPref.getCartItems(context);
                totalCost = 0.0;
                for (int a =0; a < arrayList.size(); a++){
                    ProductModel p = arrayList.get(a);

                    if (p.productDiscount > 0) {

                        totalCost = totalCost + (p.productPrice * p.itemSize);
                    }else {
                        totalCost = totalCost + (p.productPrice * p.itemSize);
                    }
                }

                btnCheckout.setText("Pay ₹ "+ totalCost);

                if (arrayList.size() == 0){
                    btnCheckout.setVisibility(View.GONE);
                }
            }
        });
        listview.setAdapter(adapter);

        for (int a =0; a < arrayList.size(); a++){
            ProductModel p = arrayList.get(a);
            totalCost = totalCost + (p.productPrice * p.itemSize);
        }

        btnCheckout.setText("Pay ₹ "+ totalCost);
    }
}