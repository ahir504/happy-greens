package com.cybilltech.happygreen.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.cybilltech.happygreen.Adapters.DealPagerAdapter;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CountDrawable;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Models.DealModel;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;

import java.util.ArrayList;

public class DealProductList extends AppCompatActivity {

    private Context context = DealProductList.this;
    private ProgressDialog progressDialog;
    private ArrayList<DealModel> categoryModelList;
    private String action;
    private static final String ProductList = "ProductList";
    private static final String CategoryModelString = "categoryList";
    private MenuItem menuItem;
    private TabLayout tabLayout;
    private ViewPager viewPagerLayout;
    private boolean isPresent = false;
    private int isPresentPosition = -1, selectedPos = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_product_list);
        init();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        menuItem = menu.findItem(R.id.menu_cart_cartItem);
        setCartCount(context, Integer.toString(MySharedPref.getCartSize(context)));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_cart_cartItem: {

                if (MySharedPref.getCartSize(context) > 0) {
                    startActivity(new Intent(context, OrderListActivity.class));
                } else {
                    CustomViews.alertDialogToast(context, "No Item in Cart", ConstantModel.FailedAlertDialog);
                }
                break;
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private void setCartCount(Context context, String count) {
        try {


            CountDrawable badge;
            LayerDrawable icon = (LayerDrawable) menuItem.getIcon();

            // Reuse drawable if possible
            Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
            if (reuse != null && reuse instanceof CountDrawable) {
                badge = (CountDrawable) reuse;
            } else {
                badge = new CountDrawable(context);
            }

            badge.setCount(count);
            icon.mutate();
            icon.setDrawableByLayerId(R.id.ic_group_count, badge);
        } catch (Exception e) {
        }
    }

    private void init() {


        viewPagerLayout = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.deal_list_tabs);
        try {
            action = getIntent().getStringExtra(ConstantModel.action);
            if (StaticClass.nullChecker(action).equals(ProductList)) {
                categoryModelList = (ArrayList<DealModel>) getIntent().getSerializableExtra(CategoryModelString);
                selectedPos = getIntent().getIntExtra("position", 0);
            }

        } catch (Exception e) {
            onBackPressed();
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(StaticClass.nullChecker(getString(R.string.app_name)));
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.colorSecondary));


        for (int k = 0; k < categoryModelList.size(); k++) {
            tabLayout.addTab(tabLayout.newTab().setText(categoryModelList.get(k).dealName));
        }


        if (categoryModelList.size() < 4) {
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        } else {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        DealPagerAdapter adapter = new DealPagerAdapter(getSupportFragmentManager(), categoryModelList);
        viewPagerLayout.setAdapter(adapter);
        viewPagerLayout.setOffscreenPageLimit(1);
        viewPagerLayout.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPagerLayout.setCurrentItem(selectedPos);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerLayout.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    public void addItemToCart(Context context, ProductModel productModel, double amount, MeasuringUnitModel measuringUnitModel){

        ArrayList<ProductModel> arrayList =  MySharedPref.getCartItems(context);

        if(isItemPresent(arrayList, productModel.productId)){
            arrayList.get(isPresentPosition).itemSize = arrayList.get(isPresentPosition).itemSize + measuringUnitModel.measCatValues;
        }else {
            productModel.itemSize = measuringUnitModel.measCatValues;
            arrayList.add(productModel);
        }

        MySharedPref.addListCartItem(context, arrayList);
        setCartCount(context, MySharedPref.getCartSize(context)+"");
        Toast.makeText(context, measuringUnitModel.measCatName +  "  " + productModel.productName + " added to cart.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        setCartCount(context, Integer.toString(MySharedPref.getCartSize(context)));
        super.onResume();
    }

    private boolean isItemPresent(ArrayList<ProductModel> arrayList, int itemId) {

        for (int a = 0; a < arrayList.size(); a++) {

            if (arrayList.get(a).productId == itemId) {
                isPresent = true;
                isPresentPosition = a;
                return true;
            }
        }
        isPresentPosition = -1;
        return false;
    }
}