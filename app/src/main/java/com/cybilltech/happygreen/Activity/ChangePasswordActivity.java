package com.cybilltech.happygreen.Activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_UserObject;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.UserModel;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.AccountService;

public class ChangePasswordActivity extends AppCompatActivity {

    private Context context = ChangePasswordActivity.this;
    private ProgressDialog progressDialog;
    private String _action = "";
    private EditText etPass, etConfPass;
    private static final String forgetPass = "forgetPass";
    private TextView tvAction;
    private Button btnChangePass;
    private int _userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        etPass = findViewById(R.id.forgetPass_et_newPass);
        etConfPass = findViewById(R.id.forgetPass_et_confPass);
        tvAction = findViewById(R.id.forgetPass_tv_action);
        btnChangePass = findViewById(R.id.forgetPass_btn_ChangePass);

        try {
            _action = StaticClass.nullChecker(getIntent().getStringExtra(ConstantModel.action));

            if (_action.equalsIgnoreCase(forgetPass)) {
                _userId = getIntent().getIntExtra("userId", 0);
            }

        } catch (Exception e) {
            _action = "";
        }

        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass = etPass.getText().toString().trim();
                String confPass = etConfPass.getText().toString().trim();

                if (pass.length() < 3) {
                    CustomViews.alertDialogToast(context, "Please enter a strong password", ConstantModel.FailedAlertDialog);
                    etPass.setError("Please enter a strong password");
                    return;
                } else if (!pass.equals(confPass)) {
                    CustomViews.alertDialogToast(context, "Password don't match", ConstantModel.FailedAlertDialog);
                    etPass.setError("Password don't match");
                    etConfPass.setError("Password don't match");
                    return;
                } else {
                    if (_action.equals(forgetPass)) {
                        changePassword();
                    }

                }

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void changePassword() {
        progressDialog.show();
        new AccountService(context).setPassword(_userId, etPass.getText().toString().trim(), new ISC_UserObject() {
            @Override
            public void onSuccess(UserModel userModel) {
                if (userModel.userId > 0) {

                    MySharedPref.setLoginStatus(context, true);
                    MySharedPref.setName(context, userModel.first_name + " " + userModel.last_name);
                    MySharedPref.setEmail(context, userModel.email);
                    MySharedPref.setUserId(context, userModel.userId);
                    MySharedPref.setMobileNo(context, userModel.phone);
                    MySharedPref.setLoginId(context, userModel.phone);

                    startActivity(new Intent(context, HomeActivity.class));
                    overridePendingTransition(R.anim.push_right_to_left_in, R.anim.push_right_to_left_out);
                    finish();
                } else {
                    CustomViews.alertDialogToast(context, "Something went wrong, Please try again", ConstantModel.FailedAlertDialog);
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                if (error.trim().length() == 0){
                    error = "No internet connectivity, Please check your internet connectivity !!!!";
                }
                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);

            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, LoginActivity.class));
        overridePendingTransition(R.anim.push_left_to_right_in, R.anim.push_left_to_right_out);
        finish();
        super.onBackPressed();
    }
}
