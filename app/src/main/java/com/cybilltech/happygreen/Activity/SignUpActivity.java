package com.cybilltech.happygreen.Activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_UserObject;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.UserModel;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.AccountService;

public class SignUpActivity extends AppCompatActivity {

    private Context context = SignUpActivity.this;
    private EditText etFirstName, etLastName, etMobileNo, etEmail, etPassword;
    private Button cvBtnSubmit;
    private TextView tvLoginLink;
    private ScrollView parentLayout;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();

        tvLoginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        cvBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etFirstName.getText().toString().trim().length() < 1) {
                    CustomViews.alertDialogToast(context, "Please enter a valid first_name", ConstantModel.FailedAlertDialog);
                    etFirstName.setError("Please enter a valid first_name");
                    return;
                } else if (etMobileNo.getText().toString().trim().length() != 10) {
                    CustomViews.alertDialogToast(context, "Please enter a valid phone No", ConstantModel.FailedAlertDialog);
                    etMobileNo.setError("Please enter a valid phone no");
                    return;
                } else if (etEmail.getText().toString().trim().length() < 5 || !etEmail.getText().toString().trim().contains("@")) {
                    CustomViews.alertDialogToast(context, "Please enter a valid email", ConstantModel.FailedAlertDialog);
                    etEmail.setError("Please enter a valid email");
                    return;
                } else if (etPassword.getText().toString().trim().length() < 3) {
                    CustomViews.alertDialogToast(context, "Please enter a valid password", ConstantModel.FailedAlertDialog);
                    etPassword.setError("Please enter a valid password");
                    return;
                } else {

                    UserModel userModel = new UserModel();
                    userModel.first_name = StaticClass.nullChecker(etFirstName.getText().toString().trim());
                    userModel.last_name = StaticClass.nullChecker(etLastName.getText().toString().trim());
                    userModel.phone = StaticClass.nullChecker(etMobileNo.getText().toString().trim());
                    userModel.email = StaticClass.nullChecker(etEmail.getText().toString().trim());
                    userModel.password = StaticClass.nullChecker(etPassword.getText().toString().trim());
                    signUp(userModel);
                }
            }
        });
    }

    private void signUp(UserModel userModel) {
        progressDialog.show();
        new AccountService(context).signUp(userModel, new ISC_UserObject() {
            @Override
            public void onSuccess(UserModel userModel) {
                progressDialog.dismiss();

                if (userModel.userId > 0) {

                    MySharedPref.setLoginStatus(context, true);
                    MySharedPref.setName(context, userModel.first_name + " " +  userModel.last_name);
                    MySharedPref.setEmail(context, userModel.email);
                    MySharedPref.setUserId(context, userModel.userId);
                    MySharedPref.setMobileNo(context, userModel.phone);
                    MySharedPref.setLoginId(context, userModel.phone);

                    startActivity(new Intent(context, HomeActivity.class));
                    overridePendingTransition(R.anim.push_right_to_left_in, R.anim.push_right_to_left_out);
                    finish();
                } else {
                    CustomViews.alertDialogToast(context, "Something went wrong, Please Try again...", ConstantModel.FailedAlertDialog);
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                if (StaticClass.nullChecker(error).length() == 0) {
                    CustomViews.alertDialogToast(context, "Your internet connection is not working. Please check your connectivity.!!!", ConstantModel.FailedAlertDialog);
                } else {
                    CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
                }
            }
        });

    }

    private void init() {

        etFirstName = findViewById(R.id.signUp_et_FirstName);
        etLastName = findViewById(R.id.signUp_et_LastName);
        etMobileNo = findViewById(R.id.signUp_et_mobileNo);
        etEmail = findViewById(R.id.signUp_et_email);
        etPassword = findViewById(R.id.signUp_et_password);
        cvBtnSubmit = findViewById(R.id.signUp_btn_BtnSubmit);
        tvLoginLink = findViewById(R.id.signUp_tv_LoginLink);
        parentLayout = findViewById(R.id.signUp_sv_patentLayout);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, LoginActivity.class));
        overridePendingTransition(R.anim.push_right_to_left_in, R.anim.push_right_to_left_out);
        finish();
    }
}
