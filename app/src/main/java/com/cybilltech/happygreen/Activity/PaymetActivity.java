package com.cybilltech.happygreen.Activity;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cybilltech.happygreen.Activity.MyOrderList;
import com.cybilltech.happygreen.Callbacks.ISC_AlertClick;
import com.cybilltech.happygreen.Callbacks.ISC_AlertClickPositiveOnly;
import com.cybilltech.happygreen.Callbacks.ISC_Click;
import com.cybilltech.happygreen.Callbacks.ISC_ClickReturnMsg;
import com.cybilltech.happygreen.Callbacks.ISC_String;
import com.cybilltech.happygreen.Callbacks.ISC_StringString;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.OrderModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.AccountService;
import com.cybilltech.happygreen.Services.ManageProductService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class PaymetActivity extends AppCompatActivity {

    private Context context = PaymetActivity.this;
    private ProgressDialog progressDialog;
    private TextView tvName, tvAddress, tvCount, tvDelivery, tvAmountPayable, tvOrderType, tvPayMode;
    private Button btnChangeAddress, btnPlaceOrder;
    private ArrayList<ProductModel> arrayList;
    private double amount = 0.0;
    private ListView lvItemNmae;
    private LinearLayout llListParent;
    private String orderType = "";
    private String paymentType = "";
    private Calendar calendar = Calendar.getInstance();
    private boolean paymentMode =false;
    private RadioGroup radioGroup1,radioGroup2;
    private RadioButton delivery,payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymet);
        init();

        btnChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomViews.showDialogwithEdit(context, "Update Address", new ISC_ClickReturnMsg() {
                    @Override
                    public void onClick(View view, final String add, final String pincode) {
                        changeAddress(add);
                    }
                });
            }
        });

        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (amount > 149.0) {
                    showDialogForOrderType();
                  //  showPaymentType();
                  //  placeOrderbuttonClicked();
                } else {
                    CustomViews.alertDialogToast(context, "Total amount should be more than ₹ 150. Then only you can place the order.\n Please add some more items or quantity ", ConstantModel.FailedAlertDialog);
                    return;
                }
            }
        });
    }
    private  void showPaymentType(){

//        int id =radioGroup2.getCheckedRadioButtonId();
//        payment=findViewById(id);
//        paymentType = payment.getText().toString();
//        Toast.makeText(context, ""+paymentType, Toast.LENGTH_SHORT).show();
//        tvPayMode.setText(paymentType);
//

        AlertDialog.Builder builder =new AlertDialog.Builder(context);
        builder.setCancelable(false);
        View view=getLayoutInflater().inflate(R.layout.custom_payment_type,null);
        builder.setView(view);
        final AlertDialog alertDialog =builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
        view.findViewById(R.id.custom_payment_type_paytm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String appPackageName = "net.one97.paytm";
                PackageManager pm = context.getPackageManager();
                Intent appstart = pm.getLaunchIntentForPackage(appPackageName);
                if(null!=appstart)
                {
                    paymentType = "Paytm";
                    alertDialog.dismiss();
                    tvPayMode.setText(paymentType);
                    paymentMode=true;
                    placeOrderbuttonClicked();
                    context.startActivity(appstart);
                }
                else {
                    CustomViews.alertDialogToast(context, "Install PayTm on your device", ConstantModel.FailedAlertDialog);
                    Toast.makeText(context, "Install PayTm on your device", Toast.LENGTH_SHORT).show();
                    paymentMode=false;
                    paymentType = "COD (Cash On Delivery)";
                    tvPayMode.setText(paymentType);
                    placeOrderbuttonClicked();
                }

            }
        });
        view.findViewById(R.id.custom_payment_type_cod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentType = "COD (Cash On Delivery)";
                alertDialog.dismiss();
                tvPayMode.setText(paymentType);
                placeOrderbuttonClicked();
            }
        });
        view.findViewById(R.id.custom_payment_type_dismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void showDialogForOrderType() {

//        int id =radioGroup1.getCheckedRadioButtonId();
//        delivery=findViewById(id);
//        orderType = delivery.getText().toString();
//        Toast.makeText(context, ""+orderType, Toast.LENGTH_SHORT).show();
//        tvOrderType.setText(orderType);

        AlertDialog.Builder  builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        View view = getLayoutInflater().inflate(R.layout.custom_order_type, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


        view.findViewById(R.id.custom_order_type_standardOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "Next Day delivery";
                alertDialog.dismiss();
                tvOrderType.setText(orderType);
                showPaymentType();
            }
        });


        view.findViewById(R.id.custom_order_type_advanceOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "Super Fast Delivery";
                alertDialog.dismiss();
                tvOrderType.setText(orderType);
                showPaymentType();
            }
        });


        view.findViewById(R.id.custom_order_type_dismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    private void placeOrderbuttonClicked() {
        String address = MySharedPref.getAddress(context);
        final OrderModel orderModel = new OrderModel();

        if (address.length() < 5) {
            CustomViews.alertDialogToast(context, "Please update your address", ConstantModel.FailedAlertDialog, new ISC_AlertClickPositiveOnly() {
                @Override
                public void onPositiveButtonClicked(View view) {
                    CustomViews.showDialogwithEdit(context, "Update Address", new ISC_ClickReturnMsg() {
                        @Override
                        public void onClick(View view, String add, String pincode) {
                            changeAddress(add);
                        }
                    });
                }
            });
        }
//        else if (amount < 0) {
//            {
//
//                CustomViews.alertDialogToast(context, "Total amount should be more than ₹ 250. Then only you can place the order.\n Please add some more items or quantity ", ConstantModel.FailedAlertDialog);
//                return;
//            }

        //}
        else {

            orderModel.OrderAmount = amount;
            orderModel.orderType = orderType;
            orderModel.paymentType = paymentType;

            orderModel.productIdList = "";
            orderModel.ProductNameList = "";
            orderModel.productQtyList = "";

            for (int a = 0; a < arrayList.size(); a++) {

                ProductModel productModel = arrayList.get(a);

                if (a == (arrayList.size() - 1)) {

                    String s = getUnitName(productModel.measuringList, productModel.itemSize);
        //            int ds = Integer.parseInt(s.replaceAll("[\\D]", ""));
                    s = s.replace(s.replaceAll("[\\D]", ""), "");

                    orderModel.productIdList = StaticClass.nullChecker(orderModel.productIdList) + productModel.productId;
                    orderModel.ProductNameList = StaticClass.nullChecker(orderModel.ProductNameList) + StaticClass.nullChecker(productModel.productName);
                    orderModel.productQtyList = StaticClass.nullChecker(orderModel.productQtyList) + StaticClass.nullChecker(productModel.itemSize + s );
                    orderModel.productQtyList2 = StaticClass.nullChecker(orderModel.productQtyList2) + productModel.itemSize;
                } else {

                    String s = getUnitName(productModel.measuringList, productModel.itemSize);
          //          int ds = Integer.parseInt(s.replaceAll("[\\D]", ""));
                    s = s.replace(s.replaceAll("[\\D]", ""), "");

                    orderModel.productIdList = StaticClass.nullChecker(orderModel.productIdList ) + productModel.productId + ", ";
                    orderModel.ProductNameList = StaticClass.nullChecker(orderModel.ProductNameList) + StaticClass.nullChecker(productModel.productName) + ", ";
                    orderModel.productQtyList = StaticClass.nullChecker(orderModel.productQtyList) + productModel.itemSize + s + " , ";
                    orderModel.productQtyList2 = StaticClass.nullChecker(orderModel.productQtyList2) + productModel.itemSize+",";

                }
            }

            CustomViews.alertDialogToast(context, "Are you sure want to place order ???", ConstantModel.SuccessAlertDialog, new ISC_AlertClick() {
                @Override
                public void onPositiveButtonClicked(View view) {
                    placeOrder(orderModel);
                }


                @Override
                public void onNegativeButtonClicked(View view) {

                }
            });
        }
    }

    private void placeOrder(final OrderModel orderModel) {
        progressDialog.show();
        new ManageProductService(context).placeOrder(orderModel, new ISC_StringString() {
            @Override
            public void onSuccess(String orderId, String msg) {
                progressDialog.dismiss();
                if (msg.equalsIgnoreCase("s")) {
                    MySharedPref.addListCartItem(context, new ArrayList<ProductModel>());

                    startActivity(new Intent(context, MyOrderList.class).putExtra(ConstantModel.activityName, "paymentSuccesful"));
                    overridePendingTransition(R.anim.push_right_to_left_in, R.anim.push_right_to_left_out);
                }

            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
            }
        });
    }

    private void init() {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(StaticClass.nullChecker("Payment"));
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.colorSecondary));

        tvName = findViewById(R.id.payment_tv_name);
        tvAddress = findViewById(R.id.payment_tv_address);
//        tvCount = findViewById(R.id.payment_tv_countNo);
//        lvItemNmae = findViewById(R.id.payment_lv_itemNameList);
        tvDelivery = findViewById(R.id.payment_tv_deleivery);
        tvAmountPayable = findViewById(R.id.payment_tv_amountPayable);
        tvOrderType = findViewById(R.id.payment_tv_orderType);
        btnChangeAddress = findViewById(R.id.payment_btn_changeAdd);
        btnPlaceOrder = findViewById(R.id.payment_btn_placeOrder);
        tvPayMode = findViewById(R.id.payment_tv_paymentMode);
        llListParent = findViewById(R.id.payment_ll_itemListParent);
        radioGroup1=findViewById(R.id.delivery_group);
        radioGroup2=findViewById(R.id.payment_type);

     //   tvPayMode.setText("Cash on Delivery");

        arrayList = MySharedPref.getCartItems(context);

        tvName.setText(StaticClass.nullChecker(MySharedPref.getName(context)).toUpperCase());
        tvAddress.setText(StaticClass.nullChecker(MySharedPref.getAddress(context)).toUpperCase() + "  (" + MySharedPref.getPinCode(context) + ")");

        llListParent.removeAllViews();
        for (int position = 0; position < arrayList.size(); position++) {
            View view = getLayoutInflater().inflate(R.layout.custom_item_price, null);

            view.setId(View.generateViewId());
            TextView tvItemName = view.findViewById(R.id.custom_itemPrice_tv_itemName);
            TextView tvItemPrice = view.findViewById(R.id.custom_itemPrice_tv_itemPrice);
            final ProductModel productModel = arrayList.get(position);
            if (productModel.productDiscount > 0) {
                tvItemPrice.setText("₹ " + String.format("%.1f", (productModel.productPrice * productModel.itemSize)));
            } else {
                tvItemPrice.setText("₹ " + String.format("%.1f", (productModel.productPrice * productModel.itemSize)));
            }
            String s = getUnitName(productModel.measuringList, productModel.itemSize);
            String unitname= "";

            for (int i=0; i<s.length(); i++)
            {
                if (!Character.isDigit(s.charAt(i))) {
                    unitname = StaticClass.nullChecker(unitname) + s.charAt(i);
                }
            }

            unitname = StaticClass.nullChecker(unitname).trim();

            String qty = productModel.itemSize + " " + StaticClass.nullChecker(unitname);
            if (productModel.itemSize < 1) {
                qty = productModel.itemSize * 1000 + " " + StaticClass.nullChecker(unitname);
            }

            tvItemName.setText(StaticClass.nullChecker(productModel.productName) + " ( " + StaticClass.nullChecker(qty) + " )");

            llListParent.addView(view);
        }

        tvDelivery.setText("Free");

        Double savedAmount = 0.0;

        for (int a = 0; a < arrayList.size(); a++) {
            ProductModel p = arrayList.get(a);
            if (p.productDiscount > 0) {
                amount = amount + (p.productPrice * p.itemSize);
                savedAmount = savedAmount + 0;
            } else {
                amount = amount + (p.productPrice * p.itemSize);
                savedAmount = savedAmount + 0;
            }
        }

        tvAmountPayable.setText("₹ " + Double.toString(amount));
        orderType = checkOrderType();
        paymentType = checkPaymentType();
        tvOrderType.setText(StaticClass.nullChecker("-"));

    }
    private String checkPaymentType(){
        String paymentType="";
        if(paymentMode){
            paymentType ="Paytm";
        }
        else{
            paymentType ="COD (Cash On Delivery)";
        }
        return paymentType;
    }

    private String checkOrderType() {
        String orderType = "";

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss", Locale.UK);

        String fromTime = "08:00:00";
        String timeTo = "19:59:59";
        String currentTime = formatter.format(calendar.getTime());

        try {
            Date dateFrom = formatter.parse(fromTime);
            Date dateto = formatter.parse(timeTo);
            Date dateCurrent = formatter.parse(currentTime);

            if (dateFrom.before(dateCurrent) && dateto.after(dateCurrent)) {
                orderType = ConstantModel.standardOrder;
            } else {
                orderType = ConstantModel.advanceOrder;
            }


        } catch (Exception e) {
            orderType = ConstantModel.unknownOrder;
        }

        return orderType;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void changeAddress(final String add) {
        progressDialog.show();
        new AccountService(context).updateAddress(MySharedPref.getEmail(context), add, new ISC_String() {
            @Override
            public void onSuccess(String msg) {
                progressDialog.dismiss();
                if (msg.equalsIgnoreCase("s")) {
                    tvAddress.setText(add + "");
                    MySharedPref.setAddress(context, add);
                } else {
                    CustomViews.alertDialogToast(context, "Try after some time", ConstantModel.FailedAlertDialog);
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
            }
        });
    }

    private String getUnitName(ArrayList<MeasuringUnitModel> models, Double itemQty) {

        for (int a = 0; a < models.size(); a++) {
            if (itemQty >= 1 && models.get(a).measCatValues == 1.0) {
                return models.get(a).measCatName;
            } else if (itemQty < 1) {
                if (models.get(a).measCatValues == 1.0) {
                    return models.get(a - 1).measCatName;
                }
            }
        }
        return "";
    }
}
