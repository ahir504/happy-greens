package com.cybilltech.happygreen.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_String;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.R;
import com.cybilltech.happygreen.Services.AccountService;

public class ProfileActivity extends AppCompatActivity {

    private Context context = ProfileActivity.this;
    private ProgressDialog progressDialog;
    private TextView tvName, tvMobile, tvMobileEdit, tvEmail, tvAddress;
    private EditText etEmail, etAddress;
    private Button btnSave;
    private LinearLayout viewLayout, editLayout;
    private ImageView ivUserIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                String address = etAddress.getText().toString();
                updatePersonalDetails(email, address);
            }
        });
    }

    private void updatePersonalDetails(final String email, final String address){

        progressDialog.show();
        new AccountService(context).updateAddress(email, address, new ISC_String() {
            @Override
            public void onSuccess(String msg) {
                progressDialog.dismiss();

                if (msg.equalsIgnoreCase("s")){

                    MySharedPref.setEmail(context, email);
                    MySharedPref.setAddress(context, address);

                    tvEmail.setText(StaticClass.nullChecker(MySharedPref.getEmail(context)));
                    tvAddress.setText(StaticClass.nullChecker(MySharedPref.getAddress(context)));
                    viewLayout.setVisibility(View.VISIBLE);
                    editLayout.setVisibility(View.GONE);

                    CustomViews.alertDialogToast(context, "Data updated successfully", ConstantModel.SuccessAlertDialog);

                }else {
                    viewLayout.setVisibility(View.VISIBLE);
                    editLayout.setVisibility(View.GONE);
                    CustomViews.alertDialogToast(context, "Data not updated, Please try after some time", ConstantModel.FailedAlertDialog);
                }

            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                viewLayout.setVisibility(View.VISIBLE);
                editLayout.setVisibility(View.GONE);
                CustomViews.alertDialogToast(context, error, ConstantModel.FailedAlertDialog);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.menu_profile_edit:{
                editMenuClicked();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void editMenuClicked() {
        viewLayout.setVisibility(View.GONE);
        editLayout.setVisibility(View.VISIBLE);
    }

    private void init() {
        getSupportActionBar().setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.colorSecondary));

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        viewLayout = findViewById(R.id.profile_view_layout);
        ivUserIcon = findViewById(R.id.iv_profile_userIcon);
        tvName = findViewById(R.id.profile_tv_name);
        tvMobile = findViewById(R.id.profile_tv_mobileNo);
        tvEmail = findViewById(R.id.profile_tv_email);
        tvAddress = findViewById(R.id.profile_tv_address);

        editLayout = findViewById(R.id.profile_edit_layout);
        tvMobileEdit = findViewById(R.id.profile_tv_mobileNoEdit);
        etEmail = findViewById(R.id.profile_et_email);
        etAddress = findViewById(R.id.profile_et_address);
        btnSave = findViewById(R.id.profile_btn_save);

        tvName.setText(StaticClass.nullChecker(MySharedPref.getName(context)).toUpperCase());
        tvMobile.setText(StaticClass.nullChecker(MySharedPref.getMobileNo(context)));
        tvEmail.setText(StaticClass.nullChecker(MySharedPref.getEmail(context)));
        tvAddress.setText(StaticClass.nullChecker(MySharedPref.getAddress(context)));

        tvMobileEdit.setText(StaticClass.nullChecker(MySharedPref.getMobileNo(context)));
        etEmail.setText(StaticClass.nullChecker(MySharedPref.getEmail(context)));
        etAddress.setText(StaticClass.nullChecker(MySharedPref.getAddress(context)));

        viewLayout.setVisibility(View.VISIBLE);
        editLayout.setVisibility(View.GONE);
    }
}
