package com.cybilltech.happygreen.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cybilltech.happygreen.Activity.DealFragment;
import com.cybilltech.happygreen.Models.DealModel;

import java.util.ArrayList;

public class DealPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<DealModel> dealModelList;

    public DealPagerAdapter(FragmentManager fm, ArrayList<DealModel> dealModelList) {
        super(fm);
        this.dealModelList=dealModelList;
    }

    @Override
    public Fragment getItem(int i) {
        return DealFragment.newInstance(dealModelList.get(i).dealName,dealModelList.get(i).arrayList);
    }

    @Override
    public int getCount() {
        return dealModelList.size();
    }
}
