package com.cybilltech.happygreen.Adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_Click;
import com.cybilltech.happygreen.Common.MySharedPref;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrderAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ProductModel> arrayList;
    private LayoutInflater layoutInflater;
    private ISC_Click callback;

    public OrderAdapter(Context context, ArrayList<ProductModel> arrayList, ISC_Click callback) {
        this.context = context;
        this.arrayList = arrayList;
        this.callback = callback;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public ProductModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return arrayList.get(position).productId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_order_item, parent, false);
        }

        CardView parentLayout = convertView.findViewById(R.id.parentCustomOrderItem);
        ImageView icon = convertView.findViewById(R.id.custom_orderItem_icon);
        TextView tvQty = convertView.findViewById(R.id.custom_orderItem_Qty);
        TextView tvName = convertView.findViewById(R.id.custom_orderItem_productName);
        TextView tvCost = convertView.findViewById(R.id.custom_orderItem_cost);
        ImageView ivCloseBtn = convertView.findViewById(R.id.custom_orderItem_closeBtn);

        parentLayout.setBackgroundResource(R.drawable.custom_order_item_background);

        final ProductModel productModel = arrayList.get(position);

        Picasso.with(context).load(arrayList.get(position).productImage).into(icon);
        tvName.setText(StaticClass.nullChecker(productModel.productName));

        //double pay = ((productModel.productPrice - ((productModel.productPrice*productModel.productDiscount)/100)) * productModel.itemSize);
        double pay = (productModel.productPrice*productModel.itemSize);

        tvCost.setText("₹ " +  String.format("%.2f", (pay)));


        String s = getUnitName(productModel.measuringList, productModel.itemSize);
        String ac  = s.replaceAll("[\\D]", "");
        if (StaticClass.nullChecker(ac).isEmpty()){
            ac = "0";
        }

        int a = Integer.parseInt(ac);

        s = s.replace(Integer.toString(a), "");
        String qty = productModel.itemSize + " " + s ;
        if (productModel.itemSize < 1){
            qty = productModel.itemSize * 1000 + " "+s;
        }

        tvQty.setText(StaticClass.nullChecker(qty));

        ivCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySharedPref.removeCartProduct(context, productModel);
                arrayList = MySharedPref.getCartItems(context);
                notifyDataSetInvalidated();
                callback.onClick(v);
            }
        });

        return convertView;
    }

    private String getUnitName(ArrayList<MeasuringUnitModel> models, Double itemQty){

        for (int a =0; a<models.size(); a++){
            if (itemQty >= 1 && models.get(a).measCatValues == 1.0){
                return  models.get(a).measCatName;
            }else if (itemQty < 1){
                if (models.get(a).measCatValues == 1.0){
                    return  models.get(a-1).measCatName;
                }
            }
        }

        return "";

    }
}
