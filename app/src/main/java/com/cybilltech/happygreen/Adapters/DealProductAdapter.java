package com.cybilltech.happygreen.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cybilltech.happygreen.Callbacks.ISC_Product_addCallback;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DealProductAdapter extends RecyclerView.Adapter<DealProductAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<ProductModel> arrayList;
    private LayoutInflater layoutInflater;
    private double _mesCatValue = 0.0;
    private int _mesCatId = 0;
    private ISC_Product_addCallback callback;
    private double payableAmount = 0.0;

    public DealProductAdapter(Context context, ArrayList<ProductModel> arrayList, ISC_Product_addCallback callback) {
        this.context = context;
        this.arrayList = arrayList;
        this.callback = callback;
        layoutInflater=LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=layoutInflater.inflate(R.layout.custom_product,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        myViewHolder.tvQtyLeft.setVisibility(View.INVISIBLE);
        final ProductModel productModel = arrayList.get(i);
//        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent=new Intent(context, ProductActivity.class);
//                intent.putExtra(ConstantModel.action,arrayList);
//                intent.putExtra("productList",arrayList);
//                intent.putExtra("productName",productModel.productName );
//                context.startActivity(intent);
//            }
//        });


        String s = getUnitName(productModel.measuringList);
        if (s.isEmpty()) {
            s = productModel.measuringList.get(0).measCatName;
        }
        String sd = s.replaceAll("[\\D]", "");
        //System.out.println(sd+" sd ");
        //int ds = Integer.parseInt(sd);
        final String unitName = (s.replace(sd, "")).trim();

        Picasso.with(context).load(productModel.productImage).into(myViewHolder.icon);
        myViewHolder.tvName.setText(StaticClass.nullChecker(productModel.productName));

        myViewHolder.tvPrice.setText(StaticClass.nullChecker("₹ " + productModel.productPrice) + " per " + unitName);
        myViewHolder.tvDis.setText(StaticClass.nullChecker(productModel.productDescription));
        myViewHolder.tvQtyLeft.setText(StaticClass.nullChecker(productModel.productQuantityLeft + " " + unitName + " Left"));

        myViewHolder.tvAmountPayable.setText(StaticClass.nullChecker("₹ " + 0));
        if (productModel.productDiscount > 0) {

            myViewHolder.tvDiscount.setText("Discount " + StaticClass.nullChecker(Integer.toString((int) productModel.productDiscount)) + " %");
            myViewHolder.tvDiscount.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.tvDiscount.setVisibility(View.GONE);
        }

        for (int a = 0; a < productModel.measuringList.size(); a++) {

            if (productModel.measuringList.get(a).measCatValues == 0.0) {
                _mesCatValue = productModel.measuringList.get(a).measCatValues;
                _mesCatId = productModel.measuringList.get(a).measCatId;
            }
            myViewHolder.tvQty.setText(_mesCatValue + " " + unitName);
        }

        productModel.measuringList.add(0, new MeasuringUnitModel(0, " - ", 0.0));



       myViewHolder. spinner.setVisibility(View.INVISIBLE);
        ArrayAdapter<MeasuringUnitModel> unitModelArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, productModel.measuringList);
        myViewHolder.spinner.setAdapter(unitModelArrayAdapter);

        myViewHolder.spinner.setSelection(arrayList.get(i).adpterSelectedPos);

        myViewHolder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int spinnerPos, long id) {
                MeasuringUnitModel measuringUnitModel = (MeasuringUnitModel) parent.getItemAtPosition(spinnerPos);

                if (spinnerPos == 0) {
                    myViewHolder.tvQty.setText(" - ");
                    myViewHolder.tvAmountPayable.setText("");
                    myViewHolder.tvBtnAddCart.setVisibility(View.GONE);
                } else {
                    myViewHolder.tvQty.setText(measuringUnitModel.measCatName);
                    payableAmount = ((productModel.productPrice  * ((MeasuringUnitModel) myViewHolder.spinner.getSelectedItem()).measCatValues));
                    //          payableAmount = (productModel.productPrice);  - ((productModel.productPrice * productModel.productDiscount) / 100))
                    myViewHolder.tvAmountPayable.setText("₹ " + Double.toString(payableAmount));
                    myViewHolder.tvBtnAddCart.setVisibility(View.VISIBLE);
                }

                arrayList.get(myViewHolder.getAdapterPosition()).adpterSelectedPos = spinnerPos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        myViewHolder.ivBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = myViewHolder.spinner.getSelectedItemPosition();

                if ((pos + 1) == myViewHolder.spinner.getCount()) {
                    Toast.makeText(context, "Maximum quantity selected", Toast.LENGTH_SHORT).show();
                } else {
                    myViewHolder.spinner.setSelection(pos + 1);
                }
            }
        });


        myViewHolder.ivBtnLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = myViewHolder.spinner.getSelectedItemPosition();

                if (pos == 0) {
                    Toast.makeText(context, "Selected quantity already 0", Toast.LENGTH_SHORT).show();
                } else {
                    myViewHolder.spinner.setSelection(pos - 1);
                }
            }
        });

        myViewHolder.tvBtnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(productModel, payableAmount, (MeasuringUnitModel) myViewHolder.spinner.getSelectedItem());
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView icon,ivBtnLess,ivBtnAdd;
        TextView tvPrice,tvName,tvDis,tvQtyLeft,tvDiscount,tvAmountPayable,tvBtnAddCart,tvQty;
        CardView cardView;
        Spinner spinner;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.custom_product_iv_icon);
            tvPrice = itemView.findViewById(R.id.custom_product_tv_price);
            tvName = itemView.findViewById(R.id.custom_product_tv_name);
            tvDis = itemView.findViewById(R.id.custom_product_tv_description);
            tvQtyLeft = itemView.findViewById(R.id.custom_product_tv_qtyLeft);

            tvDiscount = itemView.findViewById(R.id.custom_product_tv_discount);
            tvAmountPayable = itemView.findViewById(R.id.custom_product_tv_payableAmount);
            ivBtnLess = itemView.findViewById(R.id.custom_product_iv_less);
            ivBtnAdd = itemView.findViewById(R.id.custom_product_iv_add);
            tvBtnAddCart = itemView.findViewById(R.id.custom_product_tvBtn_add);
            tvQty = itemView.findViewById(R.id.custom_product_tv_Qty);
            cardView=itemView.findViewById(R.id.custom_product_item_ll_parentLayout);
            spinner = itemView.findViewById(R.id.custom_product_spinner);
        }
    }
    private String getUnitName(ArrayList<MeasuringUnitModel> models) {

        if (models != null && models.size() > 0) {
            for (int a = 0; a < models.size(); a++) {

                if (models.get(a).measCatName.contains("ml")) {
                    if (models.get(a).measCatValues == 1000.0) {
                        return models.get(a).measCatName;
                    }
                } else if (models.get(a).measCatValues == 1.0) {
                    return models.get(a).measCatName;
                }
            }
        }
        return "";
    }
}
