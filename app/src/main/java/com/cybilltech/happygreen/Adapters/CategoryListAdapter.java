package com.cybilltech.happygreen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_Click2;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.CategoryModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.Activity.ProductListActivity;
import com.cybilltech.happygreen.R;

import java.util.ArrayList;

public class CategoryListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CategoryModel> categoryList;
    private LayoutInflater layoutInflater;

    public CategoryListAdapter(Context context, ArrayList<CategoryModel> categoryList) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return categoryList.get(position).catId;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_cat_item, parent, false);
        }

        final CategoryModel categoryModel = categoryList.get(position);
            TextView tvCatName = convertView.findViewById(R.id.custom_cat_item_name);
            TextView tvBtnSeeAll = convertView.findViewById(R.id.custom_cat_item_btnSeeAll);
            RecyclerView recyclerView = convertView.findViewById(R.id.custom_cat_item_rv_productList);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

            tvCatName.setText(StaticClass.nullChecker(categoryModel.catName));


            tvBtnSeeAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (categoryModel.productList.size() > 0) {
                            Intent intent = new Intent(context, ProductListActivity.class);
                            intent.putExtra(ConstantModel.action, "ProductList");
                            intent.putExtra("categoryList", categoryList);
                            intent.putExtra("position", position);
                            context.startActivity(intent);
                        }else {
                            CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                        }
                    }catch (Exception e){
                        CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                    }

                }
            });

            ArrayList<ProductModel> productList = new ArrayList<>();

            int count = categoryModel.productList.size();

            if (count > 5){
                for (int a = (count-1); a > (count-6); a--){
                    productList.add(categoryModel.productList.get(a));
                }
            }else {
                productList = categoryModel.productList;
            }

            ProductListAdapter productListAdapter = new ProductListAdapter(0, context, productList, new ISC_Click2() {
                @Override
                public void onClick(View view, ProductModel productModel) {
                    try {
                        if (categoryModel.productList.size() > 0) {
                            Intent intent = new Intent(context, ProductListActivity.class);
                            intent.putExtra(ConstantModel.action, "ProductList");
                            intent.putExtra("categoryList", categoryList);
                            intent.putExtra("position", position);
                            context.startActivity(intent);
                        }else {
                            CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                        }
                    }catch (Exception e){
                        CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                    }
                }
            });
            recyclerView.setAdapter(productListAdapter);

        return convertView;
    }
}
