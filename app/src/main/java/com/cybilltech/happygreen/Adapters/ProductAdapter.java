package com.cybilltech.happygreen.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cybilltech.happygreen.Callbacks.ISC_Product_addCallback;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ProductModel> arrayList;
    private LayoutInflater layoutInflater;
    private double _mesCatValue = 0.0;
    private int _mesCatId = 0;
    private ISC_Product_addCallback callback;
    private double payableAmount = 0.0;

    public ProductAdapter(Context context, ArrayList<ProductModel> arrayList, ISC_Product_addCallback callback) {
        this.context = context;
        this.arrayList = arrayList;
        this.callback = callback;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public ProductModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return arrayList.get(position).productId;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.custom_product, parent, false);
        }

        ImageView icon = convertView.findViewById(R.id.custom_product_iv_icon);
        TextView tvPrice = convertView.findViewById(R.id.custom_product_tv_price);
        TextView tvName = convertView.findViewById(R.id.custom_product_tv_name);
        TextView tvDis = convertView.findViewById(R.id.custom_product_tv_description);
        TextView tvQtyLeft = convertView.findViewById(R.id.custom_product_tv_qtyLeft);
        tvQtyLeft.setVisibility(View.INVISIBLE);

        TextView tvDiscount = convertView.findViewById(R.id.custom_product_tv_discount);
        final TextView tvAmountPayable = convertView.findViewById(R.id.custom_product_tv_payableAmount);
        ImageView ivBtnLess = convertView.findViewById(R.id.custom_product_iv_less);
        ImageView ivBtnAdd = convertView.findViewById(R.id.custom_product_iv_add);
        final TextView tvBtnAddCart = convertView.findViewById(R.id.custom_product_tvBtn_add);
        final TextView tvQty = convertView.findViewById(R.id.custom_product_tv_Qty);
        CardView cardView=convertView.findViewById(R.id.custom_product_item_ll_parentLayout);

        final ProductModel productModel = arrayList.get(position);

//        cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent=new Intent(context,ProductActivity.class);
//                intent.putExtra(ConstantModel.action,arrayList);
//                intent.putExtra("productList",arrayList);
//                intent.putExtra("productName",productModel.productName );
//                context.startActivity(intent);
//            }
//        });


        String s = getUnitName(productModel.measuringList);
        if (s.isEmpty()) {
            s = productModel.measuringList.get(0).measCatName;
        }
        String sd = s.replaceAll("[\\D]", "");
        //System.out.println(sd+" sd ");
        //int ds = Integer.parseInt(sd);
        final String unitName = (s.replace(sd, "")).trim();

        Picasso.with(context).load(productModel.productImage).into(icon);
        tvName.setText(StaticClass.nullChecker(productModel.productName));

        tvPrice.setText(StaticClass.nullChecker("₹ " + productModel.productPrice) + " per " + unitName);
        tvDis.setText(StaticClass.nullChecker(productModel.productDescription));
        tvQtyLeft.setText(StaticClass.nullChecker(productModel.productQuantityLeft + " " + unitName + " Left"));

        tvAmountPayable.setText(StaticClass.nullChecker("₹ " + 0));
        if (productModel.productDiscount > 0) {

            tvDiscount.setText("Discount " + StaticClass.nullChecker(Integer.toString((int) productModel.productDiscount)) + " %");
            tvDiscount.setVisibility(View.VISIBLE);
        } else {
            tvDiscount.setVisibility(View.GONE);
        }

        for (int a = 0; a < productModel.measuringList.size(); a++) {

            if (productModel.measuringList.get(a).measCatValues == 0.0) {
                _mesCatValue = productModel.measuringList.get(a).measCatValues;
                _mesCatId = productModel.measuringList.get(a).measCatId;
            }
            tvQty.setText(_mesCatValue + " " + unitName);
        }

        productModel.measuringList.add(0, new MeasuringUnitModel(0, " - ", 0.0));


        final Spinner spinner = convertView.findViewById(R.id.custom_product_spinner);
        spinner.setVisibility(View.INVISIBLE);
        ArrayAdapter<MeasuringUnitModel> unitModelArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, productModel.measuringList);
        spinner.setAdapter(unitModelArrayAdapter);

        spinner.setSelection(arrayList.get(position).adpterSelectedPos);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int spinnerPos, long id) {
                MeasuringUnitModel measuringUnitModel = (MeasuringUnitModel) parent.getItemAtPosition(spinnerPos);

                if (spinnerPos == 0) {
                    tvQty.setText(" - ");
                    tvAmountPayable.setText("");
                    tvBtnAddCart.setVisibility(View.GONE);
                } else {
                    tvQty.setText(measuringUnitModel.measCatName);
                    payableAmount = ((productModel.productPrice  * ((MeasuringUnitModel) spinner.getSelectedItem()).measCatValues));
          //          payableAmount = (productModel.productPrice);  - ((productModel.productPrice * productModel.productDiscount) / 100))
                    tvAmountPayable.setText("₹ " + Double.toString(payableAmount));
                    tvBtnAddCart.setVisibility(View.VISIBLE);
                }

                arrayList.get(position).adpterSelectedPos = spinnerPos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ivBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = spinner.getSelectedItemPosition();

                if ((pos + 1) == spinner.getCount()) {
                    Toast.makeText(context, "Maximum quantity selected", Toast.LENGTH_SHORT).show();
                } else {
                    spinner.setSelection(pos + 1);
                }
            }
        });


        ivBtnLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = spinner.getSelectedItemPosition();

                if (pos == 0) {
                    Toast.makeText(context, "Selected quantity already 0", Toast.LENGTH_SHORT).show();
                } else {
                    spinner.setSelection(pos - 1);
                }
            }
        });

        tvBtnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(productModel, payableAmount, (MeasuringUnitModel) spinner.getSelectedItem());
            }
        });

        return convertView;
    }

    private String getUnitName(ArrayList<MeasuringUnitModel> models) {

        if (models != null && models.size() > 0) {
            for (int a = 0; a < models.size(); a++) {

                if (models.get(a).measCatName.contains("ml")) {
                    if (models.get(a).measCatValues == 1000.0) {
                        return models.get(a).measCatName;
                    }
                } else if (models.get(a).measCatValues == 1.0) {
                    return models.get(a).measCatName;
                }
            }
        }
        return "";
    }
}
