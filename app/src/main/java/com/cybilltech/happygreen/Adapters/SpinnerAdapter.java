package com.cybilltech.happygreen.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.MeasuringUnitModel;
import com.cybilltech.happygreen.R;

import java.util.ArrayList;

public class SpinnerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MeasuringUnitModel> arrayList;
    private LayoutInflater layoutInflater;


    public SpinnerAdapter(Context context, ArrayList<MeasuringUnitModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public MeasuringUnitModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return arrayList.get(position).measCatId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_single_textview, parent, false);
        }

        TextView tvName = convertView.findViewById(R.id.custom_singleTextView_tvItemName);
        tvName.setText(StaticClass.nullChecker(arrayList.get(position).measCatName));
        return convertView;
    }
}
