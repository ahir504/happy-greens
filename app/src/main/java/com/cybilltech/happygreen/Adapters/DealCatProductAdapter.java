package com.cybilltech.happygreen.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_Click2;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DealCatProductAdapter extends RecyclerView.Adapter<DealCatProductAdapter.MyViewHolder> {

    private ArrayList<ProductModel> arrayList;
    private Context context;
    private LayoutInflater layoutInflater;
    private ISC_Click2 callback;
    private int hide =0;

    public DealCatProductAdapter(int hide, Context context, ArrayList<ProductModel> arrayList, final ISC_Click2 callback){
        this.arrayList = arrayList;
        this.context = context;
        this.callback =callback;
        this.hide = hide;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.custom_product_item, viewGroup, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final ProductModel productModel = arrayList.get(holder.getAdapterPosition());

        holder.tvItemName.setText(StaticClass.nullChecker(productModel.productName));
        holder.tvItemPrice.setText(StaticClass.nullChecker("₹ "+ (productModel.productPrice)));

        if (productModel.productDiscount > 0){

            holder.tvItemDiscount.setText("Discount " + StaticClass.nullChecker( Integer.toString((int) productModel.productDiscount)) + " %" );
            holder.tvItemDiscount.setVisibility(View.VISIBLE);
        }else {
            holder.tvItemDiscount.setVisibility(View.GONE);

        }


        Picasso.with(context).load(productModel.productImage).into(holder.icon);

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(v, new ProductModel());
            }
        });

        if (hide == 0) {
            holder.addToCart.setVisibility(View.GONE);
        }else {
            holder.addToCart.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        final CardView itemLayout;
        final TextView tvItemName;
        final TextView tvItemPrice;
        final TextView tvItemDiscount;
        final TextView addToCart;
        final ImageView icon;
        MyViewHolder(View convertView) {

            super(convertView);

            itemLayout = convertView.findViewById(R.id.custom_product_item_ll_parentLayout);
            tvItemName = convertView.findViewById(R.id.custom_product_item_tv_itemName);
            tvItemPrice = convertView.findViewById(R.id.custom_product_item_tv_itemPrice);
            addToCart = convertView.findViewById(R.id.custom_product_item_tv_AddToCart);
            tvItemDiscount = convertView.findViewById(R.id.custom_product_item_tv_itemDiscount);
            icon = convertView.findViewById(R.id.custom_product_item_sdv_icon);

        }
    }
}
