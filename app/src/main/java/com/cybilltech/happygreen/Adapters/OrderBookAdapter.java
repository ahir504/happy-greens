package com.cybilltech.happygreen.Adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.Models.OrderModel;
import com.cybilltech.happygreen.R;

import java.util.ArrayList;

public class OrderBookAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<OrderModel> arrayList;
    private LayoutInflater layoutInflater;

    public OrderBookAdapter(Context context, ArrayList<OrderModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public OrderModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return arrayList.get(position).orderId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_orderbook_item, parent, false);
        }

        OrderModel orderModel = arrayList.get(position);

        CardView parentLayout = convertView.findViewById(R.id.parentLayoutCustomOrderBook);
        TextView tvOrderId = convertView.findViewById(R.id.custom_orderBookItem_orderId);
        TextView tvItemName = convertView.findViewById(R.id.custom_orderBookItem_ItemName);
        TextView tvAmount = convertView.findViewById(R.id.custom_orderBookItem_totalAmount);
        TextView tvOrderStatus = convertView.findViewById(R.id.custom_orderBookItem_orderStatus);
        TextView tvOrderType = convertView.findViewById(R.id.custom_orderBookItem_orderType);
        TextView tvAddresDeliveredTo = convertView.findViewById(R.id.custom_orderBookItem_address);
        TextView tvDate = convertView.findViewById(R.id.custom_orderBookItem_date);
        TextView tvpaymentmode = convertView.findViewById(R.id.custom_orderBookItem_payment_mode);
        TextView tvCancelReason = convertView.findViewById(R.id.custom_orderBookCencelReason);
        RelativeLayout relativeLayout= convertView.findViewById(R.id.reason_layout);


        parentLayout.setBackgroundResource(R.drawable.custom_order_item_background);

        tvOrderId.setText(""+orderModel.orderId);

//        tvItemName.setText(orderModel.ProductNameList);

         String[] productNameArray =  (orderModel.ProductNameList).split(",");
         String[] productQtyArray =  (orderModel.productQtyList).split(",");

         StringBuilder stringBuilder  = new StringBuilder();
         String itemName = "";
         if (productNameArray != null && productNameArray.length > 0){

             for (int a = 0; a < productNameArray.length; a++){

                 if (a == 0){
                     String s = productNameArray[a] + " (" + productQtyArray[a] + ")";
                     itemName = s;

                 }else {
                     String s = "\n" + productNameArray[a] + " (" + productQtyArray[a] + ")";
                     itemName = itemName+ s;
                 }
             }
         }

         tvItemName.setText(StaticClass.nullChecker(itemName));

        tvAmount.setText("₹ "+Double.toString(orderModel.OrderAmount));
        tvAddresDeliveredTo.setText(orderModel.userAddress + "  ( "+orderModel.userPinCode +" )");
        tvOrderType.setText(StaticClass.nullChecker(orderModel.orderType));
        tvpaymentmode.setText(StaticClass.nullChecker(orderModel.paymentType));



//         ||
        if (orderModel.orderStatus.equalsIgnoreCase(ConstantModel.cancelled)){
            tvOrderStatus.setText(" "+ orderModel.orderStatus +" ");
            tvOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.round_status_red));
            tvOrderStatus.setTextColor(context.getResources().getColor(R.color.colorWhite));
            tvCancelReason.setText(StaticClass.nullChecker(orderModel.cancelReason));
            relativeLayout.setVisibility(View.VISIBLE); }
        else if(orderModel.orderStatus.equalsIgnoreCase(ConstantModel.pending)) {
            relativeLayout.setVisibility(View.GONE);
            tvOrderStatus.setText(" "+ orderModel.orderStatus +" ");
            tvOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.round_status_yellow));
            tvOrderStatus.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }
        else {
            relativeLayout.setVisibility(View.GONE);
            tvOrderStatus.setText(" "+ orderModel.orderStatus +" ");
            tvOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.round_status_green));
            tvOrderStatus.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }

        tvDate.setText(StaticClass.DateFormetter("yyyy-MM-dd HH:mm:ss", "hh:mm a, dd-MM-yyyy", orderModel.orderDate));





        return convertView;
    }
}
