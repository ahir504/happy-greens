package com.cybilltech.happygreen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cybilltech.happygreen.Callbacks.ISC_Click2;
import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Activity.DealProductList;
import com.cybilltech.happygreen.Models.DealModel;
import com.cybilltech.happygreen.Models.ProductModel;
import com.cybilltech.happygreen.R;

import java.util.ArrayList;

public class DealAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<DealModel> categoryList;
    private LayoutInflater layoutInflater;

    public DealAdapter(Context context, ArrayList<DealModel> categoryList) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int i) {
        return categoryList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return categoryList.get(i).dealId;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_cat_item, parent, false);
        }

        final DealModel categoryModel = categoryList.get(position);
        TextView tvCatName = convertView.findViewById(R.id.custom_cat_item_name);
        TextView tvBtnSeeAll = convertView.findViewById(R.id.custom_cat_item_btnSeeAll);
        RecyclerView recyclerView = convertView.findViewById(R.id.custom_cat_item_rv_productList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        tvCatName.setText(StaticClass.nullChecker(categoryModel.dealName));


        tvBtnSeeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (categoryModel.arrayList.size() > 0) {
                        Intent intent = new Intent(context, DealProductList.class);
                        intent.putExtra(ConstantModel.action, "ProductList");
                        intent.putExtra("categoryList", categoryList);
                        intent.putExtra("position", position);
                        context.startActivity(intent);
                    }else {
                        CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                    }
                }catch (Exception e){
                    CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                }

            }
        });

        ArrayList<ProductModel> productList = new ArrayList<>();

        int count = categoryModel.arrayList.size();

        if (count > 5){
            for (int a = (count-1); a > (count-6); a--){
                productList.add(categoryModel.arrayList.get(a));
            }
        }else {
            productList = categoryModel.arrayList;
        }

        DealCatProductAdapter productListAdapter = new DealCatProductAdapter(0, context, productList, new ISC_Click2() {
            @Override
            public void onClick(View view, ProductModel productModel) {
                try {
                    if (categoryModel.arrayList.size() > 0) {
                        Intent intent = new Intent(context, DealProductList.class);
                        intent.putExtra(ConstantModel.action, "ProductList");
                        intent.putExtra("categoryList", categoryList);
                        intent.putExtra("position", position);
                        context.startActivity(intent);
                    }else {
                        CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                    }
                }catch (Exception e){
                    CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                }
            }
        });
        recyclerView.setAdapter(productListAdapter);

        return convertView;
    }
}
