package com.cybilltech.happygreen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybilltech.happygreen.Common.StaticClass;
import com.cybilltech.happygreen.CustomClasses.ConstantModel;
import com.cybilltech.happygreen.CustomClasses.CustomViews;
import com.cybilltech.happygreen.Models.CategoryModel;
import com.cybilltech.happygreen.Activity.ProductListActivity;
import com.cybilltech.happygreen.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RvListAdapter extends RecyclerView.Adapter<RvListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<CategoryModel> categoryList;
    private LayoutInflater layoutInflater;

    public RvListAdapter(Context context, ArrayList<CategoryModel> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
        layoutInflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater layoutInflater= LayoutInflater.from(context);
        view=layoutInflater.inflate(R.layout.custm_rv_cat_list_item,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        final CategoryModel categoryModel = categoryList.get(i);
        myViewHolder.cat_list_text.setText(StaticClass.nullChecker(categoryModel.catName));
        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (categoryModel.productList.size() > 0) {
                        Intent intent = new Intent(context, ProductListActivity.class);
                        intent.putExtra(ConstantModel.action, "ProductList");
                        intent.putExtra("categoryList", categoryList);
                        intent.putExtra("position",myViewHolder.getAdapterPosition());
                        context.startActivity(intent);
                    }else {
                        CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                    }
                }catch (Exception e){
                    CustomViews.alertDialogToast(context, "No Item in List", ConstantModel.FailedAlertDialog);
                }

            }

        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

    CircleImageView cat_list_img;
    TextView cat_list_text;
    CardView cardView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            cat_list_img=itemView.findViewById(R.id.cat_list_img);
            cat_list_text=itemView.findViewById(R.id.cat_List_text);
            cardView=itemView.findViewById(R.id.see_cat);

        }
    }


}
