package com.cybilltech.happygreen.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cybilltech.happygreen.Activity.CatFragment;
import com.cybilltech.happygreen.Models.CategoryModel;

import java.util.ArrayList;

public class CatPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<CategoryModel> categoryModelList;

    public CatPagerAdapter(FragmentManager fm, ArrayList<CategoryModel> categoryModelList) {
        super(fm);
        this.categoryModelList = categoryModelList;
    }

    @Override
    public Fragment getItem(int position) {
        return CatFragment.newInstance(categoryModelList.get(position).catName, categoryModelList.get(position).productList);
    }

    @Override
    public int getCount() {
        return categoryModelList.size();
    }
}
