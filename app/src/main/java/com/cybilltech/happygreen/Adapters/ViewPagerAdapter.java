package com.cybilltech.happygreen.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cybilltech.happygreen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class ViewPagerAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    private int  []arrayList;

    public ViewPagerAdapter(int[] arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object) ;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater= LayoutInflater.from(container.getContext());
        View view=layoutInflater.inflate(R.layout.custom_view_pager_layout,container,false);
        ImageView imageView;
        imageView=view.findViewById(R.id.pager_img);
        imageView.setImageResource(arrayList[position]);
        container.addView(view,0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}
